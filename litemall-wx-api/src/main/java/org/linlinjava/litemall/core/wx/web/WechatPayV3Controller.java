//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.linlinjava.litemall.core.wx.web;

import io.swagger.annotations.Api;
import org.linlinjava.litemall.core.domain.ExampleResult;
import org.linlinjava.litemall.core.wx.pay.v3.common.IdWorkerService;
import org.linlinjava.litemall.core.wx.pay.v3.enums.WechatPayNotifyHeader;
import org.linlinjava.litemall.core.wx.pay.v3.service.WechatNotifyDataService;
import org.linlinjava.litemall.core.wx.pay.v3.service.WechatPayV3Service;
import org.linlinjava.litemall.db.wx.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;


@Api(
        tags = {"微信V3支付"}
)
@RestController("org.linlinjava.litemall.core.wx.web.WechatPayV3Controller")
@RequestMapping({"/wechat/pay/v3"})
public class WechatPayV3Controller {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private WechatPayV3Service wechatPayService;
    @Autowired
    private WechatNotifyDataService wechatNotifyDataService;
    @Autowired
    ApplicationEventPublisher applicationEventPublisher;


    public WechatPayV3Controller() {
    }

    @PostMapping({"/jsapiOrder/{outTradeNo}"})
    public ExampleResult<PrepayBody> jsapiOrder(@PathVariable("outTradeNo") String outTradeNo) throws IOException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        return this.wechatPayService.jsAddOrder(outTradeNo);
    }

    @PostMapping({"/notify"})
    public AsyncNotifyResult asyncNotify(HttpServletRequest request) throws Exception {
        this.logger.info("微信支付回调...");
        String timestamp = request.getHeader(WechatPayNotifyHeader.WECHATPAY_TIMESTAMP.getMessage());
        String nonce = request.getHeader(WechatPayNotifyHeader.WECHATPAY_NONCE.getMessage());
        String signature = request.getHeader(WechatPayNotifyHeader.WECHATPAY_SIGNATURE.getMessage());
        String serialNo = request.getHeader(WechatPayNotifyHeader.WECHATPAY_SERIAL.getMessage());
        String requestId = request.getHeader(WechatPayNotifyHeader.WECHATPAY_REQUEST_ID.getMessage());
        String messageBody = this.readNotifyMessage(request);
        String ip = request.getRemoteAddr();
        AsyncNotifyData notifyData = new AsyncNotifyData();
        notifyData.setTimestamp(timestamp);
        notifyData.setNonce(nonce);
        notifyData.setSignature(signature);
        notifyData.setSerialNo(serialNo);
        notifyData.setMessageBody(messageBody);
        notifyData.setRequestId(requestId);
        this.wechatNotifyDataService.saveData(ip, notifyData);
        return this.wechatPayService.notifyWechatPayV3(notifyData);
    }

    private String readNotifyMessage(HttpServletRequest request) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        try {
            br = request.getReader();

            String str;
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }

            br.close();
        } catch (IOException var13) {
            var13.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException var12) {
                    var12.printStackTrace();
                }
            }

        }

        return sb.toString();
    }

    @PostMapping({"/closeOrder/{outTradeNo}"})
    public ExampleResult<?> closeOrder(@PathVariable("outTradeNo") String outTradeNo) throws IOException {
        this.wechatPayService.closeWechatOrder(outTradeNo);
        return ExampleResult.ok();
    }

    @PostMapping({"/queryOrder/{outTradeNo}"})
    public ExampleResult<PayNotifyBody> queryOrder(@PathVariable("outTradeNo") String outTradeNo) throws IOException {
        PayNotifyBody payBody = this.wechatPayService.getWechatOrderInfo(outTradeNo);
        return ExampleResult.ok(payBody);
    }

//  @PostMapping({"/refund_by_outTradeNo/{outTradeNo}"})
//  public ExampleResult<RefundBody> refundByOutTradeNo(@PathVariable("outTradeNo") String outTradeNo, @RequestParam("amount") BigDecimal amount, @RequestParam("reason") String reason) throws IOException {
//    String outRefundNo = this.wechatPayService.createRefundData(outTradeNo, reason, amount);
//    return outRefundNo == null ? ExampleResult.fail("失败") : this.refund(outRefundNo);
//  }

    @PostMapping({"/refund/{outRefundNo}"})
    public ExampleResult<RefundBody> refund(@PathVariable("outRefundNo") String outRefundNo) throws IOException {
        return this.wechatPayService.refundApply(outRefundNo);
    }

    @PostMapping({"/queryRefund/{outRefundNo}"})
    public ExampleResult<RefundResponseBody> queryRefund(@PathVariable("outRefundNo") String outRefundNo) throws IOException {
        RefundResponseBody payBody = this.wechatPayService.getWechatRefundInfo(outRefundNo);
        return ExampleResult.ok(payBody);
    }
}

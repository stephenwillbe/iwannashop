package org.linlinjava.litemall.db.wx.model;

public class RefundBody
{
  private RefundResponseBody body;
  private int statusCode;
  private String message;
  
  public RefundBody(int statusCode)
  {
    this.statusCode = statusCode;
  }
  
  public RefundBody(RefundResponseBody body, int statusCode)
  {
    this.body = body;
    this.statusCode = statusCode;
  }
  
  public int getStatusCode()
  {
    return this.statusCode;
  }
  
  public void setStatusCode(int statusCode)
  {
    this.statusCode = statusCode;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setMessage(String message)
  {
    this.message = message;
  }
  
  public RefundResponseBody getBody()
  {
    return this.body;
  }
  
  public void setBody(RefundResponseBody body)
  {
    this.body = body;
  }
}

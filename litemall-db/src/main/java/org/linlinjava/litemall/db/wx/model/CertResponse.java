package org.linlinjava.litemall.db.wx.model;

import java.util.List;

public class CertResponse
{
  private List<CertResponseListBody> data;
  
  public List<CertResponseListBody> getData()
  {
    return this.data;
  }
  
  public void setData(List<CertResponseListBody> data)
  {
    this.data = data;
  }
}

package org.linlinjava.litemall.db.wx.model;

public class WxAmount
{
  private int total;
  private String currency = "CNY";
  
  public WxAmount(int total)
  {
    this.total = total;
  }
  
  public int getTotal()
  {
    return this.total;
  }
  
  public void setTotal(int total)
  {
    this.total = total;
  }
  
  public String getCurrency()
  {
    return this.currency;
  }
  
  public void setCurrency(String currency)
  {
    this.currency = currency;
  }
}

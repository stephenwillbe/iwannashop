package org.linlinjava.litemall.db.wx.model;

public class RefundNotifyAmount
{
  private int total;
  private int refund;
  private int payer_total;
  private int payer_refund;
  
  public int getTotal()
  {
    return this.total;
  }
  
  public void setTotal(int total)
  {
    this.total = total;
  }
  
  public int getPayer_total()
  {
    return this.payer_total;
  }
  
  public void setPayer_total(int payer_total)
  {
    this.payer_total = payer_total;
  }
  
  public int getRefund()
  {
    return this.refund;
  }
  
  public void setRefund(int refund)
  {
    this.refund = refund;
  }
  
  public int getPayer_refund()
  {
    return this.payer_refund;
  }
  
  public void setPayer_refund(int payer_refund)
  {
    this.payer_refund = payer_refund;
  }
}

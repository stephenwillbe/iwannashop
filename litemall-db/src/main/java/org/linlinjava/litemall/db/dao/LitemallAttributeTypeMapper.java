package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.LitemallAttributeType;

public interface LitemallAttributeTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LitemallAttributeType record);

    int insertSelective(LitemallAttributeType record);

    LitemallAttributeType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LitemallAttributeType record);

    int updateByPrimaryKey(LitemallAttributeType record);
}
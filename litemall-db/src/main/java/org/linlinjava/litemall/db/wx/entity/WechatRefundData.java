package org.linlinjava.litemall.db.wx.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class WechatRefundData
        extends BaseUuidEntity
        implements Serializable {
    private static final long serialVersionUID = 135748851458475L;
    private String appid;
    private String transactionId;
    private String reason;
    private String outTradeNo;
    private String outRefundNo;
    private String fundsAccount;
    private String currency;
    private Integer amount;
    private Integer refundAmount;
    private String status;
    private String channel;
    private String userReceivedAccount;
    private String successTime;
    private LocalDateTime submitTime;
    private String mchid;
    private String refundId;
    private String busDataType;

    public String getAppid() {
        return this.appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getOutTradeNo() {
        return this.outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getAmount() {
        return this.amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOutRefundNo() {
        return this.outRefundNo;
    }

    public void setOutRefundNo(String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }

    public String getFundsAccount() {
        return this.fundsAccount;
    }

    public void setFundsAccount(String fundsAccount) {
        this.fundsAccount = fundsAccount;
    }

    public Integer getRefundAmount() {
        return this.refundAmount;
    }

    public void setRefundAmount(Integer refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUserReceivedAccount() {
        return this.userReceivedAccount;
    }

    public void setUserReceivedAccount(String userReceivedAccount) {
        this.userReceivedAccount = userReceivedAccount;
    }

    public String getSuccessTime() {
        return this.successTime;
    }

    public void setSuccessTime(String successTime) {
        this.successTime = successTime;
    }

    public LocalDateTime getSubmitTime() {
        return this.submitTime;
    }

    public void setSubmitTime(LocalDateTime submitTime) {
        this.submitTime = submitTime;
    }

    public String getMchid() {
        return this.mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getRefundId() {
        return this.refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getBusDataType() {
        return this.busDataType;
    }

    public void setBusDataType(String busDataType) {
        this.busDataType = busDataType;
    }
}

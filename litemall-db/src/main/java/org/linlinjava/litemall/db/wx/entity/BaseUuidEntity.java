package org.linlinjava.litemall.db.wx.entity;

import java.time.LocalDateTime;

public abstract class BaseUuidEntity {
    private Long id;
    private LocalDateTime createAt = LocalDateTime.now();

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateAt() {
        return this.createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }
}

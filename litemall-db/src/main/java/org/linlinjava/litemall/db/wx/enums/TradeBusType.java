package org.linlinjava.litemall.db.wx.enums;

public enum TradeBusType
{

  TRANSACTION("TRANSACTION", "交易支付"),
  REFUND("REFUND", "退款");

  private String code;
  private String message;
  
  public String getCode()
  {
    return this.code;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  private TradeBusType(String code, String message)
  {
    this.code = code;
    this.message = message;
  }
}

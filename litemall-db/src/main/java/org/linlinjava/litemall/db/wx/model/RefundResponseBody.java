package org.linlinjava.litemall.db.wx.model;

public class RefundResponseBody
{
  private String refund_id;
  private String channel;
  private String user_received_account;
  private String out_trade_no;
  private String out_refund_no;
  private String transaction_id;
  private String success_time;
  private String create_time;
  private String status;
  private WxRefundResponseAmount amount;
  private String funds_account;
  
  public String getRefund_id()
  {
    return this.refund_id;
  }
  
  public void setRefund_id(String refund_id)
  {
    this.refund_id = refund_id;
  }
  
  public String getChannel()
  {
    return this.channel;
  }
  
  public void setChannel(String channel)
  {
    this.channel = channel;
  }
  
  public String getUser_received_account()
  {
    return this.user_received_account;
  }
  
  public void setUser_received_account(String user_received_account)
  {
    this.user_received_account = user_received_account;
  }
  
  public String getOut_trade_no()
  {
    return this.out_trade_no;
  }
  
  public void setOut_trade_no(String out_trade_no)
  {
    this.out_trade_no = out_trade_no;
  }
  
  public String getOut_refund_no()
  {
    return this.out_refund_no;
  }
  
  public void setOut_refund_no(String out_refund_no)
  {
    this.out_refund_no = out_refund_no;
  }
  
  public String getTransaction_id()
  {
    return this.transaction_id;
  }
  
  public void setTransaction_id(String transaction_id)
  {
    this.transaction_id = transaction_id;
  }
  
  public String getSuccess_time()
  {
    return this.success_time;
  }
  
  public void setSuccess_time(String success_time)
  {
    this.success_time = success_time;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public void setCreate_time(String create_time)
  {
    this.create_time = create_time;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setStatus(String status)
  {
    this.status = status;
  }
  
  public WxRefundResponseAmount getAmount()
  {
    return this.amount;
  }
  
  public void setAmount(WxRefundResponseAmount amount)
  {
    this.amount = amount;
  }
  
  public String getFunds_account()
  {
    return this.funds_account;
  }
  
  public void setFunds_account(String funds_account)
  {
    this.funds_account = funds_account;
  }
}

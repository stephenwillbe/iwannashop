package org.linlinjava.litemall.db.wx.model;

public class WxPayer
{
  private String openid;
  
  public WxPayer(String openid)
  {
    this.openid = openid;
  }
  
  public String getOpenid()
  {
    return this.openid;
  }
  
  public void setOpenid(String openid)
  {
    this.openid = openid;
  }
}

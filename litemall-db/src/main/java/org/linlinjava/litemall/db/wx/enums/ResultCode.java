package org.linlinjava.litemall.db.wx.enums;


import org.linlinjava.litemall.db.wx.model.IErrorCode;

public enum ResultCode
  implements IErrorCode
{

  SUCCESS(200L, "操作成功"),
  FAILED(500L, "操作失败"),
  VALIDATE_FAILED(404L, "参数检验失败"),
  UNAUTHORIZED(401L, "暂未登录或token已经过期"),
  ONLY_UNAUTHORIZED(401L, "账号已在其他地点登陆"),
  ROLE_UNAUTHORIZED(401L, "用户没有绑定角色"),
  FORBIDDEN(403L, "没有相关权限");

  private final long code;
  private final String message;
  
  private ResultCode(long code, String message)
  {
    this.code = code;
    this.message = message;
  }
  
  public long getCode()
  {
    return this.code;
  }
  
  public String getMessage()
  {
    return this.message;
  }
}

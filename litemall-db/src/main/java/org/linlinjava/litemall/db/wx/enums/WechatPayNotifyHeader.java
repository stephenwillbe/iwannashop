package org.linlinjava.litemall.db.wx.enums;

public enum WechatPayNotifyHeader
{

  WECHATPAY_TIMESTAMP("Wechatpay-Timestamp"),
  WECHATPAY_NONCE("Wechatpay-Nonce"),
  WECHATPAY_SERIAL("Wechatpay-Serial"),
  WECHATPAY_REQUEST_ID("Request-ID"),
  WECHATPAY_SIGNATURE("Wechatpay-Signature");

  private String message;
  
  private WechatPayNotifyHeader(String message)
  {
    this.message = message;
  }
  
  public String getMessage()
  {
    return this.message;
  }
}

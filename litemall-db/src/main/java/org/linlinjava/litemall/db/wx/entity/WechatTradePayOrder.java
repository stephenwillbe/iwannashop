package org.linlinjava.litemall.db.wx.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class WechatTradePayOrder
  extends BaseUuidEntity
  implements Serializable
{
  private static final long serialVersionUID = 13574880051458475L;
  private String appid;
  private String mchid;
  private String description;
  private String outTradeNo;
  private String attach;
  private String currency;
  private Integer amount;
  private String payerOpenId;
  private String transactionId;
  private String tradeType;
  private String tradeState;
  private String successTime;
  private LocalDateTime submitTime;
  private String busDataType;
  private LocalDateTime timeExpire;
  private int refundAmount = 0;
  
  public String getAppid()
  {
    return this.appid;
  }
  
  public void setAppid(String appid)
  {
    this.appid = appid;
  }
  
  public String getMchid()
  {
    return this.mchid;
  }
  
  public void setMchid(String mchid)
  {
    this.mchid = mchid;
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public void setDescription(String description)
  {
    this.description = description;
  }
  
  public String getOutTradeNo()
  {
    return this.outTradeNo;
  }
  
  public void setOutTradeNo(String outTradeNo)
  {
    this.outTradeNo = outTradeNo;
  }
  
  public String getAttach()
  {
    return this.attach;
  }
  
  public void setAttach(String attach)
  {
    this.attach = attach;
  }
  
  public String getCurrency()
  {
    return this.currency;
  }
  
  public void setCurrency(String currency)
  {
    this.currency = currency;
  }
  
  public Integer getAmount()
  {
    return this.amount;
  }
  
  public void setAmount(Integer amount)
  {
    this.amount = amount;
  }
  
  public String getPayerOpenId()
  {
    return this.payerOpenId;
  }
  
  public void setPayerOpenId(String payerOpenId)
  {
    this.payerOpenId = payerOpenId;
  }
  
  public String getTransactionId()
  {
    return this.transactionId;
  }
  
  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }
  
  public String getTradeType()
  {
    return this.tradeType;
  }
  
  public void setTradeType(String tradeType)
  {
    this.tradeType = tradeType;
  }
  
  public String getTradeState()
  {
    return this.tradeState;
  }
  
  public void setTradeState(String tradeState)
  {
    this.tradeState = tradeState;
  }
  
  public String getSuccessTime()
  {
    return this.successTime;
  }
  
  public void setSuccessTime(String successTime)
  {
    this.successTime = successTime;
  }
  
  public LocalDateTime getSubmitTime()
  {
    return this.submitTime;
  }
  
  public void setSubmitTime(LocalDateTime submitTime)
  {
    this.submitTime = submitTime;
  }
  
  public String getBusDataType()
  {
    return this.busDataType;
  }
  
  public void setBusDataType(String busDataType)
  {
    this.busDataType = busDataType;
  }
  
  public LocalDateTime getTimeExpire()
  {
    return this.timeExpire;
  }
  
  public void setTimeExpire(LocalDateTime timeExpire)
  {
    this.timeExpire = timeExpire;
  }
  
  public int getRefundAmount()
  {
    return this.refundAmount;
  }
  
  public void setRefundAmount(int refundAmount)
  {
    this.refundAmount = refundAmount;
  }

  @Override
  public String toString() {
    return "WechatTradePayOrder{" +
            "appid='" + appid + '\'' +
            ", mchid='" + mchid + '\'' +
            ", description='" + description + '\'' +
            ", outTradeNo='" + outTradeNo + '\'' +
            ", attach='" + attach + '\'' +
            ", currency='" + currency + '\'' +
            ", amount=" + amount +
            ", payerOpenId='" + payerOpenId + '\'' +
            ", transactionId='" + transactionId + '\'' +
            ", tradeType='" + tradeType + '\'' +
            ", tradeState='" + tradeState + '\'' +
            ", successTime='" + successTime + '\'' +
            ", submitTime=" + submitTime +
            ", busDataType='" + busDataType + '\'' +
            ", timeExpire=" + timeExpire +
            ", refundAmount=" + refundAmount +
            "} " + super.toString();
  }
}

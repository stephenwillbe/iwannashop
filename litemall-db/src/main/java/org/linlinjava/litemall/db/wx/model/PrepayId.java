package org.linlinjava.litemall.db.wx.model;

public class PrepayId
{
  private String timeStamp;
  private String nonceStr;
  private String signType;
  private String Package;
  private String paySign;
  private String orderCode;
  private String prepayId;
  private String appId;
  
  public PrepayId(String appId, String timeStamp, String nonceStr, String signType, String package1, String paySign, String orderCode, String prepayId)
  {
    this.appId = appId;
    this.timeStamp = timeStamp;
    this.nonceStr = nonceStr;
    this.signType = signType;
    this.Package = package1;
    this.paySign = paySign;
    this.orderCode = orderCode;
    this.prepayId = prepayId;
  }
  
  public String getTimeStamp()
  {
    return this.timeStamp;
  }
  
  public void setTimeStamp(String timeStamp)
  {
    this.timeStamp = timeStamp;
  }
  
  public String getNonceStr()
  {
    return this.nonceStr;
  }
  
  public void setNonceStr(String nonceStr)
  {
    this.nonceStr = nonceStr;
  }
  
  public String getSignType()
  {
    return this.signType;
  }
  
  public void setSignType(String signType)
  {
    this.signType = signType;
  }
  
  public String getPackage()
  {
    return this.Package;
  }
  
  public void setPackage(String package1)
  {
    this.Package = package1;
  }
  
  public String getPaySign()
  {
    return this.paySign;
  }
  
  public void setPaySign(String paySign)
  {
    this.paySign = paySign;
  }
  
  public String getOrderCode()
  {
    return this.orderCode;
  }
  
  public void setOrderCode(String orderCode)
  {
    this.orderCode = orderCode;
  }
  
  public String getPrepayId()
  {
    return this.prepayId;
  }
  
  public void setPrepayId(String prepayId)
  {
    this.prepayId = prepayId;
  }
  
  public String getAppId()
  {
    return this.appId;
  }
  
  public void setAppId(String appId)
  {
    this.appId = appId;
  }
}

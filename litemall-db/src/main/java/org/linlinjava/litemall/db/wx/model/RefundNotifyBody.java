package org.linlinjava.litemall.db.wx.model;

public class RefundNotifyBody
  extends NotifyBody
{
  private String refund_id;
  private String refund_status;
  private String user_received_account;
  private String out_refund_no;
  private RefundNotifyAmount amount;
  
  public RefundNotifyAmount getAmount()
  {
    return this.amount;
  }
  
  public void setAmount(RefundNotifyAmount amount)
  {
    this.amount = amount;
  }
  
  public String getRefund_id()
  {
    return this.refund_id;
  }
  
  public void setRefund_id(String refund_id)
  {
    this.refund_id = refund_id;
  }
  
  public String getRefund_status()
  {
    return this.refund_status;
  }
  
  public void setRefund_status(String refund_status)
  {
    this.refund_status = refund_status;
  }
  
  public String getUser_received_account()
  {
    return this.user_received_account;
  }
  
  public void setUser_received_account(String user_received_account)
  {
    this.user_received_account = user_received_account;
  }
  
  public String getOut_refund_no()
  {
    return this.out_refund_no;
  }
  
  public void setOut_refund_no(String out_refund_no)
  {
    this.out_refund_no = out_refund_no;
  }
}

package org.linlinjava.litemall.db.domain;

import java.time.LocalDateTime;

public class LitemallAttribute {
    private Integer id;

    private String name;

    private Integer tpyeId;

    private LocalDateTime addTime;

    private LocalDateTime updateTime;

    private Boolean deleteFlag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTpyeId() {
        return tpyeId;
    }

    public void setTpyeId(Integer tpyeId) {
        this.tpyeId = tpyeId;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}
package org.linlinjava.litemall.db.wx.model;

public class AsyncNotifyResult
{
  private String code = "SUCCESS";
  private String message = "成功";
  
  protected AsyncNotifyResult() {}
  
  protected AsyncNotifyResult(String code, String message)
  {
    this.message = message;
    this.code = message;
  }
  
  public static AsyncNotifyResult success()
  {
    return new AsyncNotifyResult();
  }

  
  public static AsyncNotifyResult fail()
  {
    return new AsyncNotifyResult("fail", "非微信请求");
  }
  
  public String getCode()
  {
    return this.code;
  }
  
  public void setCode(String code)
  {
    this.code = code;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setMessage(String message)
  {
    this.message = message;
  }
}

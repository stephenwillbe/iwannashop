package org.linlinjava.litemall.db.wx.model;

public class PrepayBody
{
  private PrepayId prepayId;
  private int statusCode;
  private String message;
  
  public PrepayBody(int statusCode)
  {
    this.statusCode = statusCode;
  }
  
  public PrepayBody(PrepayId prepayId, int statusCode)
  {
    this.prepayId = prepayId;
    this.statusCode = statusCode;
  }
  
  public PrepayId getPrepayId()
  {
    return this.prepayId;
  }
  
  public void setPrepayId(PrepayId prepayId)
  {
    this.prepayId = prepayId;
  }
  
  public int getStatusCode()
  {
    return this.statusCode;
  }
  
  public void setStatusCode(int statusCode)
  {
    this.statusCode = statusCode;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setMessage(String message)
  {
    this.message = message;
  }
}

package org.linlinjava.litemall.db.wx.model;

public class NotifyPayer
{
  private String openid;
  
  public String getOpenid()
  {
    return this.openid;
  }
  
  public void setOpenid(String openid)
  {
    this.openid = openid;
  }
}

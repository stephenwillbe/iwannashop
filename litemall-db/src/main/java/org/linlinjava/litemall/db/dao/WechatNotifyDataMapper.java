package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.wx.entity.WechatNotifyData;

public interface WechatNotifyDataMapper
{
  int insert(WechatNotifyData paramWechatNotifyData);
}

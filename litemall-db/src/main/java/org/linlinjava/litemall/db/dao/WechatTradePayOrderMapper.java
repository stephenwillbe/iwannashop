package org.linlinjava.litemall.db.dao;

import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.wx.entity.WechatTradePayOrder;

import java.time.LocalDateTime;
import java.util.List;

public interface WechatTradePayOrderMapper
{
  int insert(WechatTradePayOrder paramWechatTradePayOrder);
  
  int updateState(@Param("appid") String paramString1, @Param("outTradeNo") String paramString2, @Param("state") String paramString3);
  
  int updateSuccess(@Param("appid") String paramString1, @Param("outTradeNo") String paramString2, @Param("state") String paramString3, @Param("successTime") String paramString4, @Param("transactionId") String paramString5);
  
  int updateSubmitState(@Param("appid") String paramString1, @Param("outTradeNo") String paramString2, @Param("state") String paramString3, @Param("submitTime") LocalDateTime paramLocalDateTime);
  
  WechatTradePayOrder selectByOutTradeNo(@Param("outTradeNo") String paramString);
  
  List<WechatTradePayOrder> selectOvertimeUnpay(@Param("appId") String paramString, @Param("overMinute") int paramInt);
  
  List<WechatTradePayOrder> selectTimeExpireUnpay(@Param("appId") String paramString);
  
  int addRefundAmount(@Param("outTradeNo") String paramString, @Param("refundAmount") int paramInt);
}

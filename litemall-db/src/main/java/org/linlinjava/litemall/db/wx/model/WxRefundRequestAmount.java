package org.linlinjava.litemall.db.wx.model;

public class WxRefundRequestAmount
{
  private int total;
  private String currency = "CNY";
  private int refund;
  
  public WxRefundRequestAmount(int total, int refund)
  {
    this.total = total;
    this.refund = refund;
  }
  
  public int getTotal()
  {
    return this.total;
  }
  
  public void setTotal(int total)
  {
    this.total = total;
  }
  
  public String getCurrency()
  {
    return this.currency;
  }
  
  public void setCurrency(String currency)
  {
    this.currency = currency;
  }
  
  public int getRefund()
  {
    return this.refund;
  }
  
  public void setRefund(int refund)
  {
    this.refund = refund;
  }
}

package org.linlinjava.litemall.db.wx.model;

public class RefundRequestBody
{
  private String reason;
  private String out_refund_no;
  private String transaction_id;
  private String notify_url;
  private WxRefundRequestAmount amount;
  private String funds_account;
  
  public RefundRequestBody(String transaction_id, String out_refund_no, String notify_url, WxRefundRequestAmount amount)
  {
    this.transaction_id = transaction_id;
    this.out_refund_no = out_refund_no;
    this.notify_url = notify_url;
    this.amount = amount;
  }
  
  public String getNotify_url()
  {
    return this.notify_url;
  }
  
  public void setNotify_url(String notify_url)
  {
    this.notify_url = notify_url;
  }
  
  public String getReason()
  {
    return this.reason;
  }
  
  public void setReason(String reason)
  {
    this.reason = reason;
  }
  
  public String getOut_refund_no()
  {
    return this.out_refund_no;
  }
  
  public void setOut_refund_no(String out_refund_no)
  {
    this.out_refund_no = out_refund_no;
  }
  
  public String getTransaction_id()
  {
    return this.transaction_id;
  }
  
  public void setTransaction_id(String transaction_id)
  {
    this.transaction_id = transaction_id;
  }
  
  public WxRefundRequestAmount getAmount()
  {
    return this.amount;
  }
  
  public void setAmount(WxRefundRequestAmount amount)
  {
    this.amount = amount;
  }
  
  public String getFunds_account()
  {
    return this.funds_account;
  }
  
  public void setFunds_account(String funds_account)
  {
    this.funds_account = funds_account;
  }
}

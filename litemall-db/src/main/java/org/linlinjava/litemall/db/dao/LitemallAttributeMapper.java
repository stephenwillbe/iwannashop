package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.LitemallAttribute;

public interface LitemallAttributeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LitemallAttribute record);

    int insertSelective(LitemallAttribute record);

    LitemallAttribute selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LitemallAttribute record);

    int updateByPrimaryKey(LitemallAttribute record);
}
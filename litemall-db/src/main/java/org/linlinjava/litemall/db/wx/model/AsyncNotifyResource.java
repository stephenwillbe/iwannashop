package org.linlinjava.litemall.db.wx.model;

public class AsyncNotifyResource
{
  private String algorithm;
  private String ciphertext;
  private String associated_data;
  private String original_type;
  private String nonce;
  
  public String getAlgorithm()
  {
    return this.algorithm;
  }
  
  public void setAlgorithm(String algorithm)
  {
    this.algorithm = algorithm;
  }
  
  public String getCiphertext()
  {
    return this.ciphertext;
  }
  
  public void setCiphertext(String ciphertext)
  {
    this.ciphertext = ciphertext;
  }
  
  public String getAssociated_data()
  {
    return this.associated_data;
  }
  
  public void setAssociated_data(String associated_data)
  {
    this.associated_data = associated_data;
  }
  
  public String getOriginal_type()
  {
    return this.original_type;
  }
  
  public void setOriginal_type(String original_type)
  {
    this.original_type = original_type;
  }
  
  public String getNonce()
  {
    return this.nonce;
  }
  
  public void setNonce(String nonce)
  {
    this.nonce = nonce;
  }
}

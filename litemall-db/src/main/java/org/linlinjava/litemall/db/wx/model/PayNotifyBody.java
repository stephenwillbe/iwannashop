package org.linlinjava.litemall.db.wx.model;

public class PayNotifyBody
  extends NotifyBody
{
  private String trade_type;
  private String trade_state;
  private String trade_state_desc;
  private String bank_type;
  private String attach;
  private NotifyPayer payer;
  private PayNotifyAmount amount;
  private NotifyScenceInfo scene_info;
  
  public NotifyPayer getPayer()
  {
    return this.payer;
  }
  
  public void setPayer(NotifyPayer payer)
  {
    this.payer = payer;
  }
  
  public PayNotifyAmount getAmount()
  {
    return this.amount;
  }
  
  public void setAmount(PayNotifyAmount amount)
  {
    this.amount = amount;
  }
  
  public NotifyScenceInfo getScene_info()
  {
    return this.scene_info;
  }
  
  public void setScene_info(NotifyScenceInfo scene_info)
  {
    this.scene_info = scene_info;
  }
  
  public String getTrade_type()
  {
    return this.trade_type;
  }
  
  public void setTrade_type(String trade_type)
  {
    this.trade_type = trade_type;
  }
  
  public String getTrade_state()
  {
    return this.trade_state;
  }
  
  public void setTrade_state(String trade_state)
  {
    this.trade_state = trade_state;
  }
  
  public String getTrade_state_desc()
  {
    return this.trade_state_desc;
  }
  
  public void setTrade_state_desc(String trade_state_desc)
  {
    this.trade_state_desc = trade_state_desc;
  }
  
  public String getBank_type()
  {
    return this.bank_type;
  }
  
  public void setBank_type(String bank_type)
  {
    this.bank_type = bank_type;
  }
  
  public String getAttach()
  {
    return this.attach;
  }
  
  public void setAttach(String attach)
  {
    this.attach = attach;
  }
}

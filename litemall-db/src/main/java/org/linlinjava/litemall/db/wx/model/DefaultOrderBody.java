package org.linlinjava.litemall.db.wx.model;

public class DefaultOrderBody
  extends MustUniFiedOrderBody
{
  private String attach;
  private String time_expire;
  
  public String getAttach()
  {
    return this.attach;
  }
  
  public void setAttach(String attach)
  {
    this.attach = attach;
  }
  
  public String getTime_expire()
  {
    return this.time_expire;
  }
  
  public void setTime_expire(String time_expire)
  {
    this.time_expire = time_expire;
  }
  
  public DefaultOrderBody(String appid, String mchid, String description, String out_trade_no, String notify_url, WxAmount amount, WxPayer payer)
  {
    super(appid, mchid, description, out_trade_no, notify_url, amount, payer);
  }
}

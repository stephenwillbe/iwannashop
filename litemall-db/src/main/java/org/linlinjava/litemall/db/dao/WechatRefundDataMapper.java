package org.linlinjava.litemall.db.dao;

import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.wx.entity.WechatRefundData;

import java.time.LocalDateTime;

public interface WechatRefundDataMapper {
    int insert(WechatRefundData paramWechatRefundData);

    int updateStatus(@Param("outRefundNo") String paramString1, @Param("state") String paramString2, @Param("userReceivedAccount") String paramString3, @Param("refundId") String paramString4);

    int updateSuccess(@Param("outRefundNo") String paramString1, @Param("state") String paramString2, @Param("successTime") String paramString3, @Param("userReceivedAccount") String paramString4, @Param("refundId") String paramString5);

    int updateProcessStatus(@Param("outRefundNo") String paramString1, @Param("state") String paramString2, @Param("submitTime") LocalDateTime paramLocalDateTime, @Param("userReceivedAccount") String paramString3, @Param("refundId") String paramString4, @Param("channel") String paramString5, @Param("fundsAccount") String paramString6);

    WechatRefundData selectByOutRefundNo(@Param("outRefundNo") String paramString);

    WechatRefundData selectById(@Param("id") Long paramString);

    WechatRefundData getByOrderOutNo(@Param("outTradeNo") String outTradeNo);
}

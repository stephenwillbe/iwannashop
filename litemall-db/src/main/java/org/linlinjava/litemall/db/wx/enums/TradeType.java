package org.linlinjava.litemall.db.wx.enums;

public enum TradeType
{

  JSAPI("JSAPI", "公众号支付"),
  NATIVE("NATIVE", "扫码支付"),
  APP("APP", "APP支付"),
  MICROPAY("MICROPAY", "付款码支付"),
  MWEB("MWEB", "H5支付"),
  FACEPAY("FACEPAY", "刷脸支付");

  private String code;
  private String message;
  
  public String getCode()
  {
    return this.code;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  private TradeType(String code, String message)
  {
    this.code = code;
    this.message = message;
  }
}

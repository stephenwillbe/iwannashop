package org.linlinjava.litemall.db.wx.model;

public class WxRefundResponseAmount
{
  private int total;
  private String currency = "CNY";
  private int refund;
  private int payer_total;
  private int payer_refund;
  private int settlement_refund;
  private int settlement_total;
  private int discount_refund;
  
  public WxRefundResponseAmount(int total, int refund)
  {
    this.total = total;
    this.refund = refund;
  }
  
  public int getTotal()
  {
    return this.total;
  }
  
  public void setTotal(int total)
  {
    this.total = total;
  }
  
  public String getCurrency()
  {
    return this.currency;
  }
  
  public void setCurrency(String currency)
  {
    this.currency = currency;
  }
  
  public int getRefund()
  {
    return this.refund;
  }
  
  public void setRefund(int refund)
  {
    this.refund = refund;
  }
  
  public int getPayer_total()
  {
    return this.payer_total;
  }
  
  public void setPayer_total(int payer_total)
  {
    this.payer_total = payer_total;
  }
  
  public int getPayer_refund()
  {
    return this.payer_refund;
  }
  
  public void setPayer_refund(int payer_refund)
  {
    this.payer_refund = payer_refund;
  }
  
  public int getSettlement_refund()
  {
    return this.settlement_refund;
  }
  
  public void setSettlement_refund(int settlement_refund)
  {
    this.settlement_refund = settlement_refund;
  }
  
  public int getSettlement_total()
  {
    return this.settlement_total;
  }
  
  public void setSettlement_total(int settlement_total)
  {
    this.settlement_total = settlement_total;
  }
  
  public int getDiscount_refund()
  {
    return this.discount_refund;
  }
  
  public void setDiscount_refund(int discount_refund)
  {
    this.discount_refund = discount_refund;
  }
}

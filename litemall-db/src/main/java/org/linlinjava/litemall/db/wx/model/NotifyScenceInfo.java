package org.linlinjava.litemall.db.wx.model;

public class NotifyScenceInfo
{
  private String device_id;
  
  public String getDevice_id()
  {
    return this.device_id;
  }
  
  public void setDevice_id(String device_id)
  {
    this.device_id = device_id;
  }
}

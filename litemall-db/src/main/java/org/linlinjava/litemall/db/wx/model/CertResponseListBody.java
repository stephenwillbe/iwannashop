package org.linlinjava.litemall.db.wx.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class CertResponseListBody
{
  @JsonProperty("effective_time")
  @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss", timezone="GMT+8")
  private LocalDateTime effective_time;
  private CertResponseEncryptCertificate encrypt_certificate;
  @JsonProperty("expire_time")
  @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss", timezone="GMT+8")
  private LocalDateTime expire_time;
  private String serial_no;
  
  public LocalDateTime getEffective_time()
  {
    return this.effective_time;
  }
  
  public void setEffective_time(LocalDateTime effective_time)
  {
    this.effective_time = effective_time;
  }
  
  public CertResponseEncryptCertificate getEncrypt_certificate()
  {
    return this.encrypt_certificate;
  }
  
  public void setEncrypt_certificate(CertResponseEncryptCertificate encrypt_certificate)
  {
    this.encrypt_certificate = encrypt_certificate;
  }
  
  public LocalDateTime getExpire_time()
  {
    return this.expire_time;
  }
  
  public void setExpire_time(LocalDateTime expire_time)
  {
    this.expire_time = expire_time;
  }
  
  public String getSerial_no()
  {
    return this.serial_no;
  }
  
  public void setSerial_no(String serial_no)
  {
    this.serial_no = serial_no;
  }
}

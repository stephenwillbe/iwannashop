package org.linlinjava.litemall.db.wx.model;

public abstract class NotifyBody
{
  private String appid;
  private String mchid;
  private String out_trade_no;
  private String transaction_id;
  private String success_time;
  
  public String getAppid()
  {
    return this.appid;
  }
  
  public void setAppid(String appid)
  {
    this.appid = appid;
  }
  
  public String getMchid()
  {
    return this.mchid;
  }
  
  public void setMchid(String mchid)
  {
    this.mchid = mchid;
  }
  
  public String getOut_trade_no()
  {
    return this.out_trade_no;
  }
  
  public void setOut_trade_no(String out_trade_no)
  {
    this.out_trade_no = out_trade_no;
  }
  
  public String getTransaction_id()
  {
    return this.transaction_id;
  }
  
  public void setTransaction_id(String transaction_id)
  {
    this.transaction_id = transaction_id;
  }
  
  public String getSuccess_time()
  {
    return this.success_time;
  }
  
  public void setSuccess_time(String success_time)
  {
    this.success_time = success_time;
  }
}

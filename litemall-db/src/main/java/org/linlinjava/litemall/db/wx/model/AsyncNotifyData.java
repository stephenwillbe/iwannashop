package org.linlinjava.litemall.db.wx.model;

import java.io.Serializable;

public class AsyncNotifyData
  implements Serializable
{
  private static final long serialVersionUID = -2542820452061125950L;
  private String requestId;
  private String timestamp;
  private String nonce;
  private String signature;
  private String serialNo;
  private String messageBody;
  
  public String getTimestamp()
  {
    return this.timestamp;
  }
  
  public void setTimestamp(String timestamp)
  {
    this.timestamp = timestamp;
  }
  
  public String getNonce()
  {
    return this.nonce;
  }
  
  public void setNonce(String nonce)
  {
    this.nonce = nonce;
  }
  
  public String getSignature()
  {
    return this.signature;
  }
  
  public void setSignature(String signature)
  {
    this.signature = signature;
  }
  
  public String getMessageBody()
  {
    return this.messageBody;
  }
  
  public void setMessageBody(String messageBody)
  {
    this.messageBody = messageBody;
  }
  
  public String getSerialNo()
  {
    return this.serialNo;
  }
  
  public void setSerialNo(String serialNo)
  {
    this.serialNo = serialNo;
  }
  
  public String getRequestId()
  {
    return this.requestId;
  }
  
  public void setRequestId(String requestId)
  {
    this.requestId = requestId;
  }
}

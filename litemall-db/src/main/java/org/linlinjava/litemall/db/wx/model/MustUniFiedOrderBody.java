package org.linlinjava.litemall.db.wx.model;

public class MustUniFiedOrderBody
{
  private String appid;
  private String mchid;
  private String description;
  private String out_trade_no;
  private String notify_url;
  private WxAmount amount;
  private WxPayer payer;
  
  public MustUniFiedOrderBody(String appid, String mchid, String description, String out_trade_no, String notify_url, WxAmount amount, WxPayer payer)
  {
    this.appid = appid;
    this.mchid = mchid;
    this.description = description;
    this.out_trade_no = out_trade_no;
    this.notify_url = notify_url;
    this.amount = amount;
    this.payer = payer;
  }
  
  public String getAppid()
  {
    return this.appid;
  }
  
  public void setAppid(String appid)
  {
    this.appid = appid;
  }
  
  public String getMchid()
  {
    return this.mchid;
  }
  
  public void setMchid(String mchid)
  {
    this.mchid = mchid;
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public void setDescription(String description)
  {
    this.description = description;
  }
  
  public String getOut_trade_no()
  {
    return this.out_trade_no;
  }
  
  public void setOut_trade_no(String out_trade_no)
  {
    this.out_trade_no = out_trade_no;
  }
  
  public String getNotify_url()
  {
    return this.notify_url;
  }
  
  public void setNotify_url(String notify_url)
  {
    this.notify_url = notify_url;
  }
  
  public WxAmount getAmount()
  {
    return this.amount;
  }
  
  public void setAmount(WxAmount amount)
  {
    this.amount = amount;
  }
  
  public WxPayer getPayer()
  {
    return this.payer;
  }
  
  public void setPayer(WxPayer payer)
  {
    this.payer = payer;
  }
}

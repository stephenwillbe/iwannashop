package org.linlinjava.litemall.db.wx.model;

public class AsyncNotifyResponseBody
{
  private String id;
  private String create_time;
  private String event_type;
  private String resource_type;
  private AsyncNotifyResource resource;
  private String summary;
  
  public String getId()
  {
    return this.id;
  }
  
  public void setId(String id)
  {
    this.id = id;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public void setCreate_time(String create_time)
  {
    this.create_time = create_time;
  }
  
  public String getEvent_type()
  {
    return this.event_type;
  }
  
  public void setEvent_type(String event_type)
  {
    this.event_type = event_type;
  }
  
  public String getResource_type()
  {
    return this.resource_type;
  }
  
  public void setResource_type(String resource_type)
  {
    this.resource_type = resource_type;
  }
  
  public AsyncNotifyResource getResource()
  {
    return this.resource;
  }
  
  public void setResource(AsyncNotifyResource resource)
  {
    this.resource = resource;
  }
  
  public String getSummary()
  {
    return this.summary;
  }
  
  public void setSummary(String summary)
  {
    this.summary = summary;
  }
}

package org.linlinjava.litemall.db.wx.model;

public class PayNotifyAmount
{
  private int total;
  private int payer_total;
  private String currency;
  private String payer_currency;
  
  public int getTotal()
  {
    return this.total;
  }
  
  public void setTotal(int total)
  {
    this.total = total;
  }
  
  public int getPayer_total()
  {
    return this.payer_total;
  }
  
  public void setPayer_total(int payer_total)
  {
    this.payer_total = payer_total;
  }
  
  public String getCurrency()
  {
    return this.currency;
  }
  
  public void setCurrency(String currency)
  {
    this.currency = currency;
  }
  
  public String getPayer_currency()
  {
    return this.payer_currency;
  }
  
  public void setPayer_currency(String payer_currency)
  {
    this.payer_currency = payer_currency;
  }
}

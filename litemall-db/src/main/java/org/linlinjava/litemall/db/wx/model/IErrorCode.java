package org.linlinjava.litemall.db.wx.model;

public abstract interface IErrorCode
{
  public abstract long getCode();
  
  public abstract String getMessage();
}

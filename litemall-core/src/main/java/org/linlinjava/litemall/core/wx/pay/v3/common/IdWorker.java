package org.linlinjava.litemall.core.wx.pay.v3.common;

public class IdWorker {
    private final long twepoch = 1451577600000L;
    private final long workerIdBits = 0L;
    private final long datacenterIdBits = 3L;
    private final long maxWorkerId = 0L;
    private final long maxDatacenterId = 7L;
    private final long sequenceBits = 10L;
    private final long workerIdShift = 10L;
    private final long datacenterIdShift = 10L;
    private final long timestampLeftShift = 13L;
    private final long sequenceMask = 1023L;
    private long workerId;
    private long datacenterId;
    private long sequence = 0L;
    private long lastTimestamp = -1L;

    public IdWorker() {
        this.workerId = 0L;
        this.datacenterId = 0L;
    }

    public IdWorker(long workerId, long datacenterId) {
        if ((workerId > 0L) || (workerId < 0L)) {
            throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", new Object[]{

                    Long.valueOf(0L)}));
        }
        if ((datacenterId > 7L) || (datacenterId < 0L)) {
            throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0", new Object[]{

                    Long.valueOf(7L)}));
        }
        this.workerId = workerId;
        this.datacenterId = datacenterId;
    }

    public synchronized long nextId() {
        long timestamp = timeGen();
        if (timestamp < this.lastTimestamp) {
            throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", new Object[]{

                    Long.valueOf(this.lastTimestamp - timestamp)}));
        }
        if (this.lastTimestamp == timestamp) {
            this.sequence = (this.sequence + 1L & 0x3FF);
            if (this.sequence == 0L) {
                timestamp = tilNextMillis(this.lastTimestamp);
            }
        } else {
            this.sequence = 0L;
        }
        this.lastTimestamp = timestamp;

        return timestamp - 1451577600000L << 13 | this.datacenterId << 10 | this.workerId << 10 | this.sequence;
    }

    protected long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    protected long timeGen() {
        return System.currentTimeMillis();
    }
}

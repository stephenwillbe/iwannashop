//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.linlinjava.litemall.core.wx.pay.v3.service;


import cn.hutool.core.lang.Assert;
import org.linlinjava.litemall.core.domain.ExampleResult;
import org.linlinjava.litemall.core.exception.ExampleException;
import org.linlinjava.litemall.core.wx.pay.v3.event.WechatPayV3FailResultEvent;
import org.linlinjava.litemall.db.wx.entity.WechatRefundData;
import org.linlinjava.litemall.db.wx.entity.WechatTradePayOrder;
import org.linlinjava.litemall.db.wx.model.*;
import org.linlinjava.litemall.core.wx.pay.v3.common.IdWorkerService;
import org.linlinjava.litemall.core.wx.pay.v3.config.WXConfig;
import org.linlinjava.litemall.core.wx.pay.v3.constant.WXPayBusConstant;
import org.linlinjava.litemall.core.wx.pay.v3.enums.*;
import org.linlinjava.litemall.core.wx.pay.v3.event.WechatPayV3RefundResultEvent;
import org.linlinjava.litemall.core.wx.pay.v3.event.WechatPayV3ResultEvent;
import org.linlinjava.litemall.core.lock.factory.RedisLockerFactory;
import org.linlinjava.litemall.core.lock.locker.Locker;
import org.linlinjava.litemall.core.wx.pay.v3.util.WxPayV3Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;

@Service
@Transactional
public class WechatPayV3Service {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private WxPayV3Util wxPayV3Util;
  @Autowired
  private WechatTradePayOrderService tradePayOrderService;
  @Autowired
  private WXConfig wxConfig;
  @Autowired
  private IdWorkerService idWorkerService;
  @Autowired
  ApplicationEventPublisher applicationEventPublisher;
  @Autowired
  private RedisLockerFactory lockerFactory;

  @Autowired
  private WechatRefundDataService refundDataService;

  private static final String OUT_REFUND_NO_PRE = "R";

  public WechatPayV3Service() {
  }

  public ExampleResult<PrepayBody> jsAddOrder(String outTradeNo) throws IOException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    WechatTradePayOrder o = this.tradePayOrderService.getByOutTradeNo(outTradeNo);
    if (o.getTradeState() != null) {
      return ExampleResult.fail("已提交，不能重复提交");
    } else if (o.getAmount() <= 0) {
      return ExampleResult.fail("支付金额需大于0");
    } else {
      PrepayBody easyBody = this.wxPayV3Util.jsapiPayPost(o);
      if (easyBody.getStatusCode() == StatusCode.CODE_SUCCESS.getCode()) {
        this.tradePayOrderService.updateSubmit(o.getAppid(), outTradeNo);
        return ExampleResult.ok(easyBody);
      } else {
        return ExampleResult.fail(easyBody.getMessage());
      }
    }
  }

  public AsyncNotifyResult notifyWechatPayV3(AsyncNotifyData nodifyData) throws IOException, GeneralSecurityException {
    AsyncNotifyResponseBody body = this.wxPayV3Util.parseNotifyResponseBody(nodifyData);
    if (body != null && body.getEvent_type() != null) {
      if (body.getEvent_type().startsWith(TradeBusType.TRANSACTION.getCode())) {
        return this.notifyForPay(body);
      } else {
        return body.getEvent_type().startsWith(TradeBusType.REFUND.getCode()) ? this.notifyForRefund(body) : AsyncNotifyResult.success();
      }
    } else {
      return AsyncNotifyResult.fail();
    }
  }

  private AsyncNotifyResult notifyForPay(AsyncNotifyResponseBody body) throws IOException, GeneralSecurityException {
    PayNotifyBody notifyBody = this.wxPayV3Util.decryptPayNotifyBody(body.getResource());
    Locker locker = this.lockerFactory.build();
    String lockKey = "lock:wechat:pay:outTradeNo:" + notifyBody.getOut_trade_no();
    if (locker.lock(lockKey, 20000L, 20, 500L)) {
      if (notifyBody.getTrade_state().equals(TradeState.TRADESTATE_SUCCESS.getCode())) {
        WechatTradePayOrder payOrder = this.tradePayOrderService.getByOutTradeNo(notifyBody.getOut_trade_no());
        if (TradeState.TRADESTATE_SUCCESS.getCode().equals(payOrder.getTradeState())) {
          return AsyncNotifyResult.success();
        }

        this.tradePayOrderService.paySuccess(notifyBody.getAppid(), notifyBody.getOut_trade_no(), notifyBody.getSuccess_time(), notifyBody.getTransaction_id());
        payOrder = this.tradePayOrderService.getByOutTradeNo(notifyBody.getOut_trade_no());
        //支付成功回调
        this.applicationEventPublisher.publishEvent(new WechatPayV3ResultEvent(payOrder.getBusDataType(), payOrder));
      } else {
        this.tradePayOrderService.updateState(notifyBody.getAppid(), notifyBody.getOut_trade_no(), notifyBody.getTrade_state());
        //支付失败回调
        this.applicationEventPublisher.publishEvent(new WechatPayV3FailResultEvent(notifyBody.getOut_trade_no()));
      }

      return AsyncNotifyResult.success();
    } else {
      this.logger.error("【微信支付回调】：获取订单锁失败，商户订单号：{}", notifyBody.getOut_trade_no());
      throw new ExampleException("支付结果处理错误");
    }
  }

  private AsyncNotifyResult notifyForRefund(AsyncNotifyResponseBody body) throws IOException, GeneralSecurityException {
    RefundNotifyBody notifyBody = this.wxPayV3Util.decryptRefundNotifyBody(body.getResource());
    Locker locker = this.lockerFactory.build();
    String lockKey = "lock:wechat:pay:outTradeNo:" + notifyBody.getOut_trade_no();
    if (locker.lock(lockKey, 20000L, 20, 500L)) {
      if (notifyBody.getRefund_status().equals(RefundStatusEnum.SUCCESS.getCode())) {
        WechatRefundData o = this.refundDataService.getByOutRefundNo(notifyBody.getOut_refund_no());
        if (RefundStatusEnum.SUCCESS.getCode().equals(o.getStatus())) {
          return AsyncNotifyResult.success();
        }

        this.refundDataService.refundSuccess(o.getOutRefundNo(), notifyBody.getSuccess_time(), notifyBody.getUser_received_account(), notifyBody.getRefund_id());
        o = this.refundDataService.getById(o.getId());
        this.tradePayOrderService.addRefundAmount(o.getOutTradeNo(), o.getRefundAmount());
        this.applicationEventPublisher.publishEvent(new WechatPayV3RefundResultEvent(o.getBusDataType(), o));
      } else {
        this.refundDataService.updateState(notifyBody.getOut_refund_no(), notifyBody.getRefund_status(), notifyBody.getUser_received_account(), notifyBody.getRefund_id());
      }

      return AsyncNotifyResult.success();
    } else {
      this.logger.error("【微信退款回调】：获取订单锁失败，商户订单号：{}", notifyBody.getOut_trade_no());
      throw new ExampleException("微信退款处理错误");
    }
  }

  /**
   * 保存本地预生成订单
   * @param outTradeNo
   * @param openId
   * @param busDataType
   * @param description
   * @param amount
   * @return
   */
  public WechatTradePayOrder createJsapiOrder(String outTradeNo,String openId, String busDataType, String description, BigDecimal amount) {
    if (amount != null && amount.compareTo(BigDecimal.ZERO) > 0) {
      WechatTradePayOrder o = new WechatTradePayOrder();
      o.setAmount(amount.multiply(WXPayBusConstant.BIGDECIMAL_100).intValue());
      o.setAppid(this.wxConfig.getAppId());
      o.setCreateAt(LocalDateTime.now());
      o.setCurrency(TradeCurrencyEnum.CNY.getCode());
      o.setDescription(description);
      o.setPayerOpenId(openId);
      o.setOutTradeNo(outTradeNo);
      o.setMchid(this.wxConfig.getMchId());
      o.setTradeType(TradeType.JSAPI.getCode());
      o.setBusDataType(busDataType);
      o.setAttach(busDataType);
      o.setTimeExpire(LocalDateTime.now().plusMinutes(60L));
      this.tradePayOrderService.saveData(o);
      return o;
    } else {
      return null;
    }
  }

  public String buildOutTradeNo() {
    return this.wxConfig.getMchId() + this.idWorkerService.nextId();
  }

  public void closeWechatOrder(String outTradeNo) throws IOException {
    this.wxPayV3Util.closeOrder(outTradeNo);
  }

  public PayNotifyBody getWechatOrderInfo(String outTradeNo) throws IOException {
    return this.wxPayV3Util.queryOrderInfo(outTradeNo);
  }

  /**
   * 自动更新订单状态
   * @throws IOException
   */
  public void autoUpdateOrderState() throws IOException {
    List<WechatTradePayOrder> lst = this.tradePayOrderService.listTimeExpireUnpay(this.wxConfig.getAppId());
    Iterator<WechatTradePayOrder> var3 = lst.iterator();

    while(var3.hasNext()) {
      WechatTradePayOrder o = var3.next();
      PayNotifyBody payBody = this.getWechatOrderInfo(o.getOutTradeNo());
      if (payBody != null) {
        String state = payBody.getTrade_state();
        if (state != null && !"".equals(state)) {
          if (TradeState.TRADESTATE_SUCCESS.getCode().equals(state)) {
            this.tradePayOrderService.paySuccess(o.getAppid(), o.getOutTradeNo(), payBody.getSuccess_time(), payBody.getTransaction_id());
            o = this.tradePayOrderService.getByOutTradeNo(o.getOutTradeNo());
            //支付成功订单回调
            this.applicationEventPublisher.publishEvent(new WechatPayV3ResultEvent(o.getBusDataType(), o));
          } else if (TradeState.TRADESTATE_NOTPAY.getCode().equals(state)) {
            this.closeWechatOrder(o.getOutTradeNo());
            this.tradePayOrderService.updateState(o.getAppid(), o.getOutTradeNo(), TradeState.TRADESTATE_CLOSED.getCode());
          } else {
            this.tradePayOrderService.updateState(o.getAppid(), o.getOutTradeNo(), state);
          }
        }
      }
    }

  }

  public String createRefundData(String outRefundNo,String outTradeNo, String reason, BigDecimal amount) {
    Assert.notNull(outTradeNo, "商户订单号不能为空");
    Assert.notNull(amount, "退款金额不能为空");
    Assert.isTrue(amount.compareTo(BigDecimal.ZERO) > 0, "退款金额不能小于等于0");
    WechatTradePayOrder order = this.tradePayOrderService.getByOutTradeNo(outTradeNo);
    Assert.notNull(order, "商户订单号的支付订单不存在");
    Assert.isTrue(TradeState.TRADESTATE_SUCCESS.getCode().equals(order.getTradeState()), "商户订单号的支付订单未成功");
    int refundAmount = amount.multiply(WXPayBusConstant.BIGDECIMAL_100).intValue();
    Assert.isTrue(refundAmount <= order.getAmount() - order.getRefundAmount(), "退款金额不能大于未退款的支付金额：" + (order.getAmount() - order.getRefundAmount()));
    WechatRefundData o = new WechatRefundData();
    o.setAmount(order.getAmount());
    o.setRefundAmount(refundAmount);
    o.setReason(reason);
    o.setAppid(order.getAppid());
    o.setMchid(order.getMchid());
    o.setCreateAt(LocalDateTime.now());
    o.setCurrency(order.getCurrency());
    o.setOutTradeNo(outTradeNo);
    o.setTransactionId(order.getTransactionId());
    o.setOutRefundNo(outRefundNo);
    o.setBusDataType(order.getBusDataType());
    this.refundDataService.saveData(o);
    return o.getOutRefundNo();
  }

  public String buildOutRefundNo() {
    return OUT_REFUND_NO_PRE + this.wxConfig.getMchId() + this.idWorkerService.nextId();
  }

  public ExampleResult<RefundBody> refundApply(String outRefundNo) throws IOException {
    WechatRefundData o = this.refundDataService.getByOutRefundNo(outRefundNo);
    if (o.getStatus() != null) {
      return ExampleResult.fail("已提交，不能重复提交");
    } else if (o.getAmount() <= 0) {
      return ExampleResult.fail("退款金额需大于0");
    } else {
      RefundBody easyBody = this.wxPayV3Util.refundApply(o);
      if (easyBody.getStatusCode() == StatusCode.CODE_SUCCESS.getCode()) {
        RefundResponseBody body = easyBody.getBody();
        if (RefundStatusEnum.SUCCESS.getCode().equals(body.getStatus())) {
          this.refundDataService.refundSuccess(outRefundNo, body.getSuccess_time(), body.getUser_received_account(), body.getRefund_id());
          o = this.refundDataService.getById(o.getId());
          this.tradePayOrderService.addRefundAmount(o.getOutTradeNo(), o.getRefundAmount());
          this.applicationEventPublisher.publishEvent(new WechatPayV3RefundResultEvent(o.getBusDataType(), o));
          return ExampleResult.ok(easyBody);
        } else if (RefundStatusEnum.PROCESSING.getCode().equals(body.getStatus())) {
          this.refundDataService.updateProcess(body.getOut_refund_no(), body.getUser_received_account(), body.getRefund_id(), body.getChannel(), body.getFunds_account());
          return ExampleResult.ok(easyBody);
        } else {
          this.refundDataService.updateState(outRefundNo, body.getStatus(), body.getUser_received_account(), body.getRefund_id());
          return ExampleResult.ok(easyBody);
        }
      } else {
        return ExampleResult.fail(easyBody.getMessage());
      }
    }
  }

  public RefundResponseBody getWechatRefundInfo(String outRefundNo) throws IOException {
    return this.wxPayV3Util.queryRefundBody(outRefundNo);
  }
}

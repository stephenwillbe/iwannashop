package org.linlinjava.litemall.core.lock.factory;

import lombok.extern.slf4j.Slf4j;
import org.linlinjava.litemall.core.lock.locker.Locker;
import org.linlinjava.litemall.core.lock.locker.ReentrantLocker;
import org.springframework.stereotype.Component;

/**
 * @author cjh
 * @Date: 2021/12/7
 */
@Component
@Slf4j
public class ReentrantLockerFactory implements LockerFactory {

    private static final ReentrantLocker LOCKER = new ReentrantLocker();

    @Override
    public Locker build() {
        log.debug("构建reentrantLocker");
        return LOCKER;
    }
}

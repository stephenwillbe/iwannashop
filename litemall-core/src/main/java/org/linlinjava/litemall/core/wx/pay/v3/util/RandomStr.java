package org.linlinjava.litemall.core.wx.pay.v3.util;

import java.util.Random;

public class RandomStr
{
  private static final String TEMPLATE_STR = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
  private static final int STR_LENGTH = 32;
  
  public static String getRandomStr()
  {
    return getRandomStr(32);
  }
  
  public static String getRandomStr(int strLength)
  {
    Random random = new Random();
    StringBuilder result = new StringBuilder();
    int length = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".length();
    for (int a = 0; a < strLength; a++) {
      result.append("1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".charAt(random.nextInt(length)));
    }
    return result.toString();
  }
}

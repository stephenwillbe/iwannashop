package org.linlinjava.litemall.core.lock.locker;

/**
 * @author cjh
 * @Date: 2021/12/7
 */
public abstract class BaseLocker implements Locker {

    public BaseLocker() {
    }

    @Override
    public boolean lock(String key) {
        return this.lock(key, 3000L, 6, 500L);
    }

    @Override
    public boolean lock(String key, int retryTimes) {
        return this.lock(key, 3000L, retryTimes, 500L);
    }

    @Override
    public boolean lock(String key, int retryTimes, long sleepMillis) {
        return this.lock(key, 3000L, retryTimes, sleepMillis);
    }

    @Override
    public boolean lock(String key, long expire) {
        return this.lock(key, expire, 6, 500L);
    }

    @Override
    public boolean lock(String key, long expire, int retryTimes) {
        return this.lock(key, expire, retryTimes, 500L);
    }



}

package org.linlinjava.litemall.core.wx.pay.v3.constant;

import java.math.BigDecimal;

public class WXPayBusConstant
{
  public static final BigDecimal BIGDECIMAL_100 = new BigDecimal("100");
  public static final int JSAPI_ORDER_EXPIRE_MINUTE = 60;
  public static final int CHECK_ORDER_MINUTE = 10;
  public static final String WECHAT_PAY_PUBLIC_CERTIFICATE_SERIALNO_REDIS_KEY = "wechat:payv3:public:certificate:serialNo:";
  public static final String WECHAT_PAY_OUT_TRADE_NO_LOCK = "lock:wechat:pay:outTradeNo:";
}

package org.linlinjava.litemall.core.wx.pay.v3.event;

import org.springframework.context.ApplicationEvent;

public class WechatPayV3FailResultEvent
  extends ApplicationEvent
{
  private static final long serialVersionUID = 133024388388873L;

  public WechatPayV3FailResultEvent(Object source)
  {
    super(source);
  }

  public String getOrderTradeNo()
  {
    return (String)this.source;
  }
}

package org.linlinjava.litemall.core.wx.pay.v3.task;

import org.linlinjava.litemall.core.wx.pay.v3.util.WxPayV3Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;

@Configuration
@EnableScheduling
public class TaskDownloadCert
{
  @Autowired
  private WxPayV3Util wxPayUtil;

  @Scheduled(cron="${wx.pay.update_cert_cron}")
  @PostConstruct
  public void refreshCert()
  {
    try
    {
      this.wxPayUtil.autoDownloadUpdatePublicKeyCert();
    }
    catch (Exception e)
    {
      System.err.println("更新微信支付证书失败!");
      e.printStackTrace();
    }
  }
}

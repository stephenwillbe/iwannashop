package org.linlinjava.litemall.core.wx.pay.v3.enums;

public enum TradeCurrencyEnum
{
  CNY("CNY", "人民币");

  private String code;
  private String message;
  
  public String getCode()
  {
    return this.code;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  private TradeCurrencyEnum(String code, String message)
  {
    this.code = code;
    this.message = message;
  }
}

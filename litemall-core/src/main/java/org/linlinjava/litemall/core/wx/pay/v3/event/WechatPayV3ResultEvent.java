package org.linlinjava.litemall.core.wx.pay.v3.event;

import org.linlinjava.litemall.db.wx.entity.WechatTradePayOrder;
import org.springframework.context.ApplicationEvent;

public class WechatPayV3ResultEvent
  extends ApplicationEvent
{
  private static final long serialVersionUID = 133024388388871L;
  private String busType;
  
  public WechatPayV3ResultEvent(String busType, Object source)
  {
    super(source);
    this.busType = busType;
  }
  
  public String getBusType()
  {
    return this.busType;
  }
  
  public void setBusType(String busType)
  {
    this.busType = busType;
  }
  
  public WechatTradePayOrder getPayOrder()
  {
    return (WechatTradePayOrder)this.source;
  }
}

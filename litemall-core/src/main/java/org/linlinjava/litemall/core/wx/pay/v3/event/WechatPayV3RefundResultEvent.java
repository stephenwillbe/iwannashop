package org.linlinjava.litemall.core.wx.pay.v3.event;

import org.linlinjava.litemall.db.wx.entity.WechatRefundData;
import org.springframework.context.ApplicationEvent;

public class WechatPayV3RefundResultEvent
        extends ApplicationEvent {
    private static final long serialVersionUID = 13302434588388871L;
    private String busType;

    public WechatPayV3RefundResultEvent(String busType, Object source) {
        super(source);
    }

    public String getBusType() {
        return this.busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public WechatRefundData getRefundData() {
        return (WechatRefundData) this.source;
    }
}

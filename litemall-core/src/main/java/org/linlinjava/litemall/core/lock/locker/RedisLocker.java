package org.linlinjava.litemall.core.lock.locker;

import lombok.extern.slf4j.Slf4j;
import org.linlinjava.litemall.core.wx.pay.v3.common.IdWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.types.Expiration;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import java.util.Collections;

/**
 * @author cjh
 * @Date: 2021/12/7
 */
@Slf4j
public class RedisLocker extends BaseLocker {

    private final Logger logger = LoggerFactory.getLogger(RedisLocker.class);

    private static final String UNLOCK_LUA;
    private StringRedisTemplate redisTemplate;
    private ThreadLocal<String> lockFlag = new ThreadLocal();
    private final IdWorker idWorker = new IdWorker();

    public RedisLocker(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
        Assert.notNull(redisTemplate, "请设置RedisTemplate");
    }

    @Override
    public boolean lock(String key, long expire, int retryTimes, long sleepMillis) {
        boolean result;
        for(result = this.getLock(key, expire); !result && retryTimes-- > 0; result = this.getLock(key, expire)) {
            try {
                this.logger.debug("获取Redis锁失败, 重试[{}]次.", retryTimes);
                Thread.sleep(sleepMillis);
            } catch (InterruptedException var9) {
                return false;
            }
        }

        return result;
    }

    private boolean getLock(String key, long expire) {
        try {
            long l = idWorker.nextId();
            String randomValue = String.valueOf(l);
            this.logger.debug("尝试获取Redis锁，KEY：{}，VALUE:{}", key, randomValue);
            this.lockFlag.set(randomValue);
            Boolean returnValue = (Boolean)this.redisTemplate.execute(new RedisCallback<Boolean>() {
                @Override
                public Boolean doInRedis(@NotNull RedisConnection connection) throws DataAccessException {
                    return connection.set(key.getBytes(), randomValue.getBytes(), Expiration.milliseconds(expire),
                            RedisStringCommands.SetOption.SET_IF_ABSENT);
                }
            });
            boolean result = returnValue != null && returnValue;
            if (result) {
                this.logger.debug("获取Redis锁成功，KEY：{}，VALUE:{}", key, randomValue);
            }

            return result;
        } catch (Exception var7) {
            this.logger.error("获取Redis锁失败", var7);
            return false;
        }
    }

    @Override
    public boolean release(String key) {
        try {
            String arg = (String)this.lockFlag.get();
            this.logger.debug("尝试释放Redis锁，KEY:{}， VALUE:{}.", key, arg);
            DefaultRedisScript<Long> script = new DefaultRedisScript(UNLOCK_LUA);
            script.setResultType(Long.class);
            Long returnValue = (Long)this.redisTemplate.execute(script, Collections.singletonList(key), new Object[]{arg});
            boolean result = returnValue != null && returnValue > 0L;
            if (result) {
                this.logger.debug("释放Redis锁成功，KEY:{}， VALUE:{}.", key, arg);
            } else {
                this.logger.debug("释放Redis锁失败，KEY:{}， VALUE:{}.", key, arg);
            }

            return result;
        } catch (Exception var6) {
            this.logger.error("释放Redis锁发生异常", var6);
            return false;
        }
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("if redis.call(\"get\",KEYS[1]) == ARGV[1] ");
        sb.append("then ");
        sb.append("    return redis.call(\"del\",KEYS[1]) ");
        sb.append("else ");
        sb.append("    return 0 ");
        sb.append("end ");
        UNLOCK_LUA = sb.toString();
    }

}

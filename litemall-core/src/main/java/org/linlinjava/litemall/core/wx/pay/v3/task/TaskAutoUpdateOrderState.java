package org.linlinjava.litemall.core.wx.pay.v3.task;

import org.linlinjava.litemall.core.wx.pay.v3.service.WechatPayV3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class TaskAutoUpdateOrderState
{
  @Autowired
  private WechatPayV3Service payService;
  
  @Scheduled(cron="${wx.pay.update_order_state_cron}")
  public void run()
  {
    try
    {
      this.payService.autoUpdateOrderState();
    }
    catch (Exception e)
    {
      System.err.println("更新支付订单状态失败!");
      e.printStackTrace();
    }
  }
}

package org.linlinjava.litemall.core.wx.pay.v3.constant;

public class WXPayConstant
{
  public static final String HOST = "https://api.mch.weixin.qq.com";
  public static final String JSAPI_ORDER_URL = "https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi";
  public static final String APPAPI_ORDER_URL = "https://api.mch.weixin.qq.com/v3/pay/transactions/app";
  public static final String H5_ORDER_URL = "https://api.mch.weixin.qq.com/v3/pay/transactions/h5";
  public static final String CLOSE_ORDER_URL = "https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/{out_trade_no}/close";
  public static final String QUERY_ORDER_TRADE_NO_URL = "https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/{out_trade_no}";
  public static final String GET_CERT_URL = "https://api.mch.weixin.qq.com/v3/certificates";
  public static final String REFUNDS_URL = "https://api.mch.weixin.qq.com/v3/refund/domestic/refunds";
  public static final String QUERY_REFUND_URL = "https://api.mch.weixin.qq.com/v3/refund/domestic/refunds/{out_refund_no}";
  public static final String TRADE_BILL_URL = "https://api.mch.weixin.qq.com/v3/bill/tradebill";
  public static final String FUND_FLOW_BILL_URL = "https://api.mch.weixin.qq.com/v3/bill/fundflowbill";
}

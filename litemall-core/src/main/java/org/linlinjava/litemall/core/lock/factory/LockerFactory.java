package org.linlinjava.litemall.core.lock.factory;

import org.linlinjava.litemall.core.lock.locker.Locker;

/**
 * ClassName: LockerFactory <br/>
 * Description: <br/>
 * date: 2021/12/7 14:43<br/>
 *
 * @author lz   <br/>
 * @since JDK 1.8
 */
public interface LockerFactory {

    /**
     * 获取锁对象
     *
     * @return*/
    Locker build();

}

package org.linlinjava.litemall.core.wx.pay.v3.enums;

public enum RefundStatusEnum {

    SUCCESS("SUCCESS", "退款成功"),
    PROCESSING("PROCESSING", "退款处理中"),
    CLOSED("CLOSED", "已关闭"),
    ABNORMAL("ABNORMAL", "退款异常");

    private String code;
    private String message;

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    private RefundStatusEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }
}

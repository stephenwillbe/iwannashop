package org.linlinjava.litemall.core.wx.pay.v3.common;

import org.springframework.stereotype.Component;

@Component
public class IdWorkerService {
    private IdWorker idWorker;

    public IdWorkerService() {
        this.idWorker = new IdWorker();
    }

    public long nextId() {
        return this.idWorker.nextId();
    }
}

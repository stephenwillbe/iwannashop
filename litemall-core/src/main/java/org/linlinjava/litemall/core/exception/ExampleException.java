package org.linlinjava.litemall.core.exception;

/**
 * @description:
 * @author: chenjh
 * @create: 2022-03-18
 **/
public class ExampleException extends RuntimeException {
    private static final long serialVersionUID = -4459264856354295224L;

    public ExampleException() {
    }

    public ExampleException(String msg) {
        super(msg);
    }

    public ExampleException(Throwable t) {
        super(t);
    }

    public ExampleException(String msg, Throwable t) {
        super(msg, t);
    }
}
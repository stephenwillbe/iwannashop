package org.linlinjava.litemall.core.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @description:
 * @author: chenjh
 * @create: 2022-03-18
 **/
public class ExampleResult<T> implements Serializable {

    private static final long serialVersionUID = -8189520564623761867L;
    private boolean ok;
    private String message;
    private T data;
    private Map<String, Object> moreData;

    public ExampleResult() {
    }

    public ExampleResult(boolean ok, String message, T data) {
        this.ok = ok;
        this.message = message;
        this.data = data;
    }

    public boolean isOk() {
        return this.ok;
    }

    public ExampleResult<T> setOk(boolean ok) {
        this.ok = ok;
        return this;
    }

    public String getMessage() {
        return this.message;
    }

    public ExampleResult<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return this.data;
    }

    public ExampleResult<T> setData(T data) {
        this.data = data;
        return this;
    }

    public Map<String, Object> getMoreData() {
        return this.moreData;
    }

    public ExampleResult<T> setMoreData(Map<String, Object> extData) {
        this.moreData = extData;
        return this;
    }

    public ExampleResult<T> addMoreData(String key, Object value) {
        if (this.moreData == null) {
            this.moreData = new LinkedHashMap();
        }

        this.moreData.put(key, value);
        return this;
    }

    public static <T> ExampleResult<T> ok() {
        return new ExampleResult(true, (String)null, (Object)null);
    }

    public static <T> ExampleResult<T> ok(T data) {
        return new ExampleResult(true, (String)null, data);
    }

    public static <T> ExampleResult<T> fail() {
        return new ExampleResult(false, (String)null, (Object)null);
    }

    public static <T> ExampleResult<T> fail(String message) {
        return new ExampleResult(false, message, (Object)null);
    }

    public static <T> ExampleResult<T> of(boolean ok, String message) {
        return new ExampleResult(ok, message, (Object)null);
    }

    public static <T> ExampleResult<T> of(boolean ok, T data, String message) {
        return new ExampleResult(ok, message, data);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }


}

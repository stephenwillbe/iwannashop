package org.linlinjava.litemall.core.lock.locker;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author cjh
 * @Date: 2021/12/7
 */
@Slf4j
public class ReentrantLocker extends BaseLocker {

    private static final ReentrantLock LOCK = new ReentrantLock();

    public ReentrantLocker() {
    }

    @Override
    public boolean lock(String key, long expire, int retryTimes, long sleepMillis) {
        try {
            for(boolean result = LOCK.tryLock(expire, TimeUnit.MILLISECONDS); !result && retryTimes-- > 0; result = LOCK.tryLock(expire, TimeUnit.MILLISECONDS)) {
                try {
                    log.debug("获取Reentrant锁失败, 重试[{}]次.", retryTimes);
                    Thread.sleep(sleepMillis);
                } catch (InterruptedException var9) {
                    return false;
                }
            }
        } catch (InterruptedException var10) {
            log.error("获取Reentrant锁失败", var10);
        }

        return false;
    }

    @Override
    public boolean release(String key) {
        LOCK.unlock();
        return true;
    }
}

package org.linlinjava.litemall.core.wx.pay.v3.service;

import org.linlinjava.litemall.core.wx.pay.v3.common.IdWorkerService;
import org.linlinjava.litemall.db.dao.WechatTradePayOrderMapper;
import org.linlinjava.litemall.db.wx.entity.WechatTradePayOrder;
import org.linlinjava.litemall.core.wx.pay.v3.enums.TradeState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class WechatTradePayOrderService
{
  @Autowired
  private WechatTradePayOrderMapper mapper;

  @Autowired
  private IdWorkerService idWorkerService;


  public void saveData(WechatTradePayOrder orderData)
  {
    orderData.setCreateAt(LocalDateTime.now());
    orderData.setId(idWorkerService.nextId());
    this.mapper.insert(orderData);
  }
  
  public void updateState(String appId, String outTradeNo, String state)
  {
    this.mapper.updateState(appId, outTradeNo, state);
  }
  
  public void paySuccess(String appId, String outTradeNo, String successTime, String transactionId)
  {
    this.mapper.updateSuccess(appId, outTradeNo, TradeState.TRADESTATE_SUCCESS.getCode(), successTime, transactionId);
  }
  
  public WechatTradePayOrder getByOutTradeNo(String outTradeNo)
  {
    return this.mapper.selectByOutTradeNo(outTradeNo);
  }
  
  public void updateSubmit(String appId, String outTradeNo)
  {
    this.mapper.updateSubmitState(appId, outTradeNo, TradeState.TRADESTATE_NOTPAY.getCode(), LocalDateTime.now());
  }
  
  public List<WechatTradePayOrder> listOvertimeUnpay(String appId)
  {
    return this.mapper.selectOvertimeUnpay(appId, 10);
  }
  
  public List<WechatTradePayOrder> listTimeExpireUnpay(String appId)
  {
    return this.mapper.selectTimeExpireUnpay(appId);
  }
  
  public void addRefundAmount(String outTradeNo, int refundAmount)
  {
    if (refundAmount <= 0) {
      return;
    }
    this.mapper.addRefundAmount(outTradeNo, refundAmount);
  }
}

package org.linlinjava.litemall.core.wx.pay.v3.service;

import org.linlinjava.litemall.core.wx.pay.v3.common.IdWorkerService;
import org.linlinjava.litemall.db.dao.WechatNotifyDataMapper;
import org.linlinjava.litemall.db.wx.entity.WechatNotifyData;
import org.linlinjava.litemall.db.wx.model.AsyncNotifyData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class WechatNotifyDataService
{
  @Autowired
  private WechatNotifyDataMapper mapper;
  @Autowired
  private IdWorkerService idWorkerService;



  public void saveData(String ip, AsyncNotifyData notifyData)
  {
    WechatNotifyData o = new WechatNotifyData();
    o.setId(idWorkerService.nextId());
    o.setIp(ip);
    o.setMessageBody(notifyData.getMessageBody());
    o.setNonce(notifyData.getNonce());
    o.setSignature(notifyData.getSignature());
    o.setTimestamp(notifyData.getTimestamp());
    o.setCreateAt(LocalDateTime.now());
    o.setSerialNo(notifyData.getSerialNo());
    o.setRequestId(notifyData.getRequestId());
    
    this.mapper.insert(o);
  }
}

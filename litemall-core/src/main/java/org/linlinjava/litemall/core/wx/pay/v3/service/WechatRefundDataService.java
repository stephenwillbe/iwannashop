package org.linlinjava.litemall.core.wx.pay.v3.service;

import org.linlinjava.litemall.core.wx.pay.v3.common.IdWorkerService;
import org.linlinjava.litemall.db.dao.WechatRefundDataMapper;
import org.linlinjava.litemall.db.wx.entity.WechatRefundData;
import org.linlinjava.litemall.core.wx.pay.v3.enums.RefundStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class WechatRefundDataService
{
  @Autowired
  private WechatRefundDataMapper mapper;

  @Autowired
  private IdWorkerService idWorkerService;


  public void saveData(WechatRefundData orderData)
  {
    orderData.setCreateAt(LocalDateTime.now());
    orderData.setId(idWorkerService.nextId());
    this.mapper.insert(orderData);
  }
  
  public void updateState(String outRefundNo, String state, String userReceivedAccount, String refundId)
  {
    this.mapper.updateStatus(outRefundNo, state, userReceivedAccount, refundId);
  }
  
  public void refundSuccess(String outRefundNo, String successTime, String userReceivedAccount, String refundId)
  {
    this.mapper.updateSuccess(outRefundNo, RefundStatusEnum.SUCCESS.getCode(), successTime, userReceivedAccount, refundId);
  }
  
  public WechatRefundData getByOutRefundNo(String outRefundNo)
  {
    return this.mapper.selectByOutRefundNo(outRefundNo);
  }
  
  public void updateProcess(String outRefundNo, String userReceivedAccount, String refundId, String channel, String fundsAccount)
  {
    this.mapper.updateProcessStatus(outRefundNo, RefundStatusEnum.PROCESSING.getCode(), LocalDateTime.now(), userReceivedAccount, refundId, channel, fundsAccount);
  }
  
  public WechatRefundData getById(Long id)
  {
    return this.mapper.selectById(id);
  }

  public WechatRefundData getByOutOrderNo(String outOrderNo) {
    return this.mapper.getByOrderOutNo(outOrderNo);
  }
}

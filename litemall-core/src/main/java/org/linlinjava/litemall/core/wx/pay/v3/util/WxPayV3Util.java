package org.linlinjava.litemall.core.wx.pay.v3.util;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wechat.pay.contrib.apache.httpclient.WechatPayHttpClientBuilder;
import com.wechat.pay.contrib.apache.httpclient.auth.AutoUpdateCertificatesVerifier;
import com.wechat.pay.contrib.apache.httpclient.auth.PrivateKeySigner;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Credentials;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Validator;
import com.wechat.pay.contrib.apache.httpclient.util.PemUtil;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.linlinjava.litemall.db.wx.entity.WechatRefundData;
import org.linlinjava.litemall.db.wx.entity.WechatTradePayOrder;
import org.linlinjava.litemall.db.wx.model.*;

import org.linlinjava.litemall.core.wx.pay.v3.config.WXConfig;
import org.linlinjava.litemall.core.wx.pay.v3.enums.StatusCode;
import org.linlinjava.litemall.core.wx.pay.v3.enums.TradeState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class WxPayV3Util
{
  @Autowired
  private WXConfig wxConfig;
  @Autowired
  private RedisTemplate<String, Object> redisTemplate;
  private Logger log = LoggerFactory.getLogger(WxPayV3Util.class);

//  private PrivateKey getPrivateKeyByStr(String content)
//  {
//    try
//    {
//      String privateKey = content.replace("-----BEGIN PRIVATE KEY-----", "")
//        .replace("-----END PRIVATE KEY-----", "")
//        .replaceAll("\\s+", "");
//
//      KeyFactory kf = KeyFactory.getInstance("RSA");
//
//      return kf.generatePrivate(
//        new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey)));
//    }
//    catch (NoSuchAlgorithmException e)
//    {
//      throw new RuntimeException("当前Java环境不支持RSA", e);
//    }
//    catch (InvalidKeySpecException e)
//    {
//      throw new RuntimeException("无效的密钥格式");
//    }
//  }

  public String privateSign(String sign6)
    throws NoSuchAlgorithmException, InvalidKeyException, SignatureException
  {
    PrivateKey privateKey = getPrivateKey();

    Signature sign = Signature.getInstance("SHA256withRSA");

    sign.initSign(privateKey);

    sign.update(sign6.getBytes(StandardCharsets.UTF_8));

    return Base64.getEncoder().encodeToString(sign.sign());
  }

  public CloseableHttpClient wxClient()
  {
    PrivateKey merchantPrivateKey = getPrivateKey();

    AutoUpdateCertificatesVerifier verifier = new AutoUpdateCertificatesVerifier(
      new WechatPay2Credentials(this.wxConfig.getMchId(),
      new PrivateKeySigner(this.wxConfig.getSerialNo(),
      merchantPrivateKey)), this.wxConfig.getApiv3Key().getBytes(StandardCharsets.UTF_8));

    return WechatPayHttpClientBuilder.create()
      .withMerchant(this.wxConfig.getMchId(), this.wxConfig.getSerialNo(), merchantPrivateKey)
      .withValidator(new WechatPay2Validator(verifier)).build();
  }

  private AsyncNotifyResponseBody parseNotifyMessage(String message)
    throws GeneralSecurityException, IOException
  {
    AsyncNotifyResponseBody wxResponseBody = (AsyncNotifyResponseBody)JSONObject.parseObject(message, AsyncNotifyResponseBody.class);
    return wxResponseBody;
  }

  public PayNotifyBody decryptPayNotifyBody(AsyncNotifyResource resource)
    throws GeneralSecurityException, IOException
  {
    AesUtil aesUtil = new AesUtil(this.wxConfig.getApiv3Key().getBytes(StandardCharsets.UTF_8));

    String s = aesUtil.decryptToString(resource.getAssociated_data().getBytes(StandardCharsets.UTF_8),
      resource.getNonce().getBytes(StandardCharsets.UTF_8),
      resource.getCiphertext());

    return (PayNotifyBody)JSONObject.parseObject(s, PayNotifyBody.class);
  }

  public RefundNotifyBody decryptRefundNotifyBody(AsyncNotifyResource resource)
    throws GeneralSecurityException, IOException
  {
    AesUtil aesUtil = new AesUtil(this.wxConfig.getApiv3Key().getBytes(StandardCharsets.UTF_8));

    String s = aesUtil.decryptToString(resource.getAssociated_data().getBytes(StandardCharsets.UTF_8),
      resource.getNonce().getBytes(StandardCharsets.UTF_8),
      resource.getCiphertext());

    return (RefundNotifyBody)JSONObject.parseObject(s, RefundNotifyBody.class);
  }

  public Boolean checkNotifyBody(PayNotifyBody notifyBody)
  {
    if ((notifyBody.getAppid() == null) || (!notifyBody.getAppid().equals(this.wxConfig.getAppId()))) {
      return Boolean.valueOf(false);
    }
    if ((notifyBody.getMchid() == null) || (!notifyBody.getMchid().equals(this.wxConfig.getMchId()))) {
      return Boolean.valueOf(false);
    }
    if ((notifyBody.getTrade_state() != null) && (TradeState.TRADESTATE_SUCCESS.getCode().equals(notifyBody.getTrade_state()))) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }

  public HttpPost postHeader(HttpPost httpPost)
  {
    httpPost.setHeader("Content-Type", "application/json");
    httpPost.setHeader("Accept", "application/json");
    return httpPost;
  }

  public HttpGet postHeader(HttpGet httpPost)
  {
    httpPost.setHeader("Content-Type", "application/json");
    httpPost.setHeader("Accept", "application/json");
    return httpPost;
  }

  public PublicKey getCertPublicKey(String serialNo)
    throws CertificateException, IOException
  {
    String cert = getPublicCert(serialNo);
    if (cert == null)
    {
      autoDownloadUpdatePublicKeyCert();
      cert = getPublicCert(serialNo);
    }
    return getCertPublicKeyByCertStr(getPublicCert(serialNo));
  }

  private PublicKey getCertPublicKeyByCertStr(String strCert)
    throws CertificateException, IOException
  {
    byte[] bytes = strCert.getBytes(StandardCharsets.UTF_8);

    ByteArrayInputStream basis = new ByteArrayInputStream(bytes);
    CertificateFactory certificatefactory = CertificateFactory.getInstance("X.509");

    X509Certificate cert = null;
    try
    {
      cert = (X509Certificate)certificatefactory.generateCertificate(basis);
    }
    catch (CertificateException e)
    {
      this.log.error(e.toString());
    }
    assert (cert != null);
    PublicKey publicKey = cert.getPublicKey();
    basis.close();
    return publicKey;
  }

  public boolean verify(String serialNo, String timestamp, String nonce, String message, String signedData)
    throws CertificateException, IOException
  {
    String srcData = (String)Stream.of(new String[] { timestamp, nonce, message }).collect(Collectors.joining("", "", ""));

    PublicKey publicKey = getCertPublicKey(serialNo);
    if ((signedData == null) || (publicKey == null)) {
      return false;
    }
    try
    {
      Signature sign = Signature.getInstance("SHA256withRSA");
      sign.initVerify(publicKey);
      sign.update(srcData.getBytes(StandardCharsets.UTF_8));
      return sign.verify(Base64.getDecoder().decode(signedData));
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return false;
  }

  public String buildeOrderJson(WechatTradePayOrder o)
  {
    DefaultOrderBody mustUniFiedOrderBody = new DefaultOrderBody(this.wxConfig.getAppId(), this.wxConfig.getMchId(), o.getDescription(), o.getOutTradeNo(), this.wxConfig.getNotifyUrl(), new WxAmount(o.getAmount().intValue()), new WxPayer(o.getPayerOpenId()));

    mustUniFiedOrderBody.setAttach(o.getAttach());
    if (o.getTimeExpire() != null)
    {
      ZonedDateTime zonedDateTimeInUTC = o.getTimeExpire().atZone(ZoneId.of("+8"));
      mustUniFiedOrderBody.setTime_expire(zonedDateTimeInUTC.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")) + "+08:00");
    }
    return JSON.toJSONString(mustUniFiedOrderBody);
  }

  public HttpPost getPostHttp(String url)
  {
    return postHeader(new HttpPost(url));
  }

  public void closeOrder(String orderCode)
    throws IOException
  {
    String url = getCloseOrderUrl(orderCode);
    CloseableHttpClient client = wxClient();
    HttpPost post = postHeader(new HttpPost(url));
    String jsonStr = "{\"mchid\": \"" + this.wxConfig.getMchId() + "\"}";
    StringEntity entity = new StringEntity(jsonStr, StandardCharsets.UTF_8);
    post.setEntity(entity);
    client.execute(post);
    client.close();
  }

  public String getWxOrderStatus(String orderCode)
    throws IOException
  {
    return queryOrderInfo(orderCode).getTrade_state();
  }

  public PayNotifyBody queryOrderInfo(String orderCode)
    throws IOException
  {
    CloseableHttpClient client = wxClient();
    String url = getQueryOrderUrl(orderCode);
    HttpGet post = postHeader(new HttpGet(url));
    CloseableHttpResponse response = client.execute(post);
    String g1 = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
    PayNotifyBody wxResponseBody = (PayNotifyBody)JSONObject.parseObject(g1, PayNotifyBody.class);
    return wxResponseBody;
  }

  public PrepayBody jsapiPayPost(WechatTradePayOrder o)
    throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException
  {
    String jsonStr = buildeOrderJson(o);

    CloseableHttpClient client = wxClient();

    HttpPost httpPost = getPostHttp("https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi");

    httpPost.setEntity(new StringEntity(jsonStr, StandardCharsets.UTF_8));

    CloseableHttpResponse response = client.execute(httpPost);

    String g1 = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);

    client.close();

    int statusCode = response.getStatusLine().getStatusCode();

    response.close();
    if (statusCode == StatusCode.CODE_SUCCESS.getCode())
    {
      JSONObject jsonObject = JSON.parseObject(g1);

      String prepayId = jsonObject.getString("prepay_id");

      String timeStampStr = String.valueOf(System.currentTimeMillis() / 1000L);

      String randomStr = RandomStr.getRandomStr();

      String rawSign = this.wxConfig.getAppId() + "" +
        timeStampStr + "" +
        randomStr + "" +
        "prepay_id=" + prepayId + "";

      String sign = privateSign(rawSign);

      PrepayId result = new PrepayId(this.wxConfig.getAppId(),
        timeStampStr,
        randomStr,
        "RSA",
        "prepay_id=" + prepayId,
        sign, o.getOutTradeNo(), prepayId);
      return new PrepayBody(result, statusCode);
    }
    this.log.error(g1);
    PrepayBody p = new PrepayBody(statusCode);
    JSONObject jsonObject = JSON.parseObject(g1);
    p.setMessage(jsonObject.getString("message"));
    return p;
  }

  public AsyncNotifyResponseBody parseNotifyResponseBody(AsyncNotifyData nodifyData)
    throws GeneralSecurityException, IOException
  {
    boolean verify = verify(nodifyData.getSerialNo(), nodifyData.getTimestamp(), nodifyData.getNonce(), nodifyData.getMessageBody(), nodifyData.getSignature());
    Assert.isTrue(verify, "微信签名验证错误:非微信发送");

    return parseNotifyMessage(nodifyData.getMessageBody());
  }

  private String getCloseOrderUrl(String orderCode)
  {
    return "https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/{out_trade_no}/close".replace("{out_trade_no}", orderCode);
  }

  private String getQueryOrderUrl(String outTradeNo)
  {
    return "https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/{out_trade_no}".replace("{out_trade_no}", outTradeNo) + "?mchid=" + this.wxConfig.getMchId();
  }

  public PrivateKey getPrivateKey()
  {
    try
    {
//      if ((this.wxConfig.getPrivateKey() != null) && (!"".equals(this.wxConfig.getPrivateKey().trim()))) {
//        return getPrivateKeyByStr(this.wxConfig.getPrivateKey());
//      }
      return getPrivateKeyByFile(this.wxConfig.getPrivateCertPath());
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    return null;
  }

  private PrivateKey getPrivateKeyByFile(String certFilePath)
    throws IOException
  {
    File file = new File(certFilePath);
    InputStream is = new FileInputStream(file);
    return PemUtil.loadPrivateKey(is);
  }

  public String getPublicCert(String serialNo)
  {
    return (String)this.redisTemplate.opsForValue().get("wechat:payv3:public:certificate:serialNo:" + serialNo);
  }

  public void autoDownloadUpdatePublicKeyCert()
  {
    try
    {
      CloseableHttpClient client = wxClient();
      HttpGet get = new WxPayV3Util().postHeader(new HttpGet("https://api.mch.weixin.qq.com/v3/certificates"));
      CloseableHttpResponse response = client.execute(get);
      HttpEntity resEntity = response.getEntity();
      String g1 = EntityUtils.toString(resEntity, StandardCharsets.UTF_8);
      client.close();
      response.close();
      CertResponse certResponse = (CertResponse)JSONObject.parseObject(g1, CertResponse.class);

      List<CertResponseListBody> data = certResponse.getData();
      CertResponseEncryptCertificate resource = null;
      AesUtil aesUtil = new AesUtil(this.wxConfig.getApiv3Key().getBytes(StandardCharsets.UTF_8));
      for (CertResponseListBody a : data)
      {
        String serialNo = a.getSerial_no();
        if (!redisPublicCertIsExists(serialNo))
        {
          LocalDateTime expireTime = a.getExpire_time();
          resource = a.getEncrypt_certificate();
          String cert = aesUtil.decryptToString(resource.getAssociated_data().getBytes(StandardCharsets.UTF_8),
            resource.getNonce().getBytes(StandardCharsets.UTF_8), resource.getCiphertext());
          redisAddPublicCert(serialNo, cert, expireTime);
        }
      }
    }
    catch (IOException|GeneralSecurityException e)
    {
      e.printStackTrace();
    }
  }

  private void redisAddPublicCert(String serialNo, String cert, LocalDateTime expireTime)
  {
    String key = "wechat:payv3:public:certificate:serialNo:" + serialNo;
    this.redisTemplate.opsForValue().set(key, cert);

    long nowTime = System.currentTimeMillis();
    long expireMilli = expireTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
    this.redisTemplate.expire(key, expireMilli - nowTime, TimeUnit.MILLISECONDS);
  }

  private boolean redisPublicCertIsExists(String serialNo)
  {
    return this.redisTemplate.hasKey("wechat:payv3:public:certificate:serialNo:" + serialNo).booleanValue();
  }

  public RefundBody refundApply(WechatRefundData o)
    throws IOException
  {
    String jsonStr = buildeRefundJson(o);

    CloseableHttpClient client = wxClient();

    HttpPost httpPost = getPostHttp("https://api.mch.weixin.qq.com/v3/refund/domestic/refunds");

    httpPost.setEntity(new StringEntity(jsonStr, StandardCharsets.UTF_8));

    CloseableHttpResponse response = client.execute(httpPost);

    String g1 = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);

    client.close();

    int statusCode = response.getStatusLine().getStatusCode();
    response.close();
    if (statusCode == StatusCode.CODE_SUCCESS.getCode())
    {
      RefundResponseBody respBody = (RefundResponseBody)JSONObject.parseObject(g1, RefundResponseBody.class);
      RefundBody b = new RefundBody(respBody, statusCode);
      return b;
    }
    this.log.error(g1);
    RefundBody p = new RefundBody(statusCode);
    JSONObject jsonObject = JSON.parseObject(g1);
    p.setMessage(jsonObject.getString("message"));
    return p;
  }

  public String buildeRefundJson(WechatRefundData o)
  {
    RefundRequestBody body = new RefundRequestBody(o.getTransactionId(), o.getOutRefundNo(), this.wxConfig.getNotifyUrl(), new WxRefundRequestAmount(o.getAmount().intValue(), o.getRefundAmount().intValue()));

    body.setReason(o.getReason());
    body.setFunds_account(o.getFundsAccount());

    return JSON.toJSONString(body);
  }

  private String getQueryRefundUrl(String outRefundNo)
  {
    return "https://api.mch.weixin.qq.com/v3/refund/domestic/refunds/{out_refund_no}".replace("{out_refund_no}", outRefundNo);
  }

  public RefundResponseBody queryRefundBody(String outRefundNo)
    throws IOException
  {
    CloseableHttpClient client = wxClient();
    String url = getQueryRefundUrl(outRefundNo);
    HttpGet post = postHeader(new HttpGet(url));
    CloseableHttpResponse response = client.execute(post);
    String g1 = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
    RefundResponseBody wxResponseBody = (RefundResponseBody)JSONObject.parseObject(g1, RefundResponseBody.class);
    return wxResponseBody;
  }
}

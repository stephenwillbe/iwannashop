package org.linlinjava.litemall.core.wx.pay.v3.enums;

public enum TradeState
{

  TRADESTATE_SUCCESS("SUCCESS", "支付成功"),
  TRADESTATE_REFUND("REFUND", "转入退款"),
  TRADESTATE_NOTPAY("NOTPAY", "未支付"),
  TRADESTATE_CLOSED("CLOSED", "已关闭"),
  TRADESTATE_USERPAYING("USERPAYING", "用户支付中（付款码支付）"),
  TRADESTATE_REVOKED("REVOKED", "已撤销（付款码支付）"),
  TRADESTATE_PAYERROR("PAYERROR", "支付失败（仅适用于付款码支付）");

  private String code;
  private String message;
  
  public String getCode()
  {
    return this.code;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  private TradeState(String code, String message)
  {
    this.code = code;
    this.message = message;
  }
}

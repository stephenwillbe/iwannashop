package org.linlinjava.litemall.core.wx.pay.v3.enums;

public enum StatusCode
{

  CODE_SUCCESS(200, "回应状态200");
  private final int code;
  private final String message;
  
  private StatusCode(int code, String message)
  {
    this.code = code;
    this.message = message;
  }
  
  public int getCode()
  {
    return this.code;
  }
  
  public String getMessage()
  {
    return this.message;
  }
}

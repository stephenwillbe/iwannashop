package org.linlinjava.litemall.core.lock.factory;

import lombok.extern.slf4j.Slf4j;
import org.linlinjava.litemall.core.lock.locker.Locker;
import org.linlinjava.litemall.core.lock.locker.RedisLocker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @author cjh
 * @Date: 2021/12/7
 */
@Component
@Slf4j
public class RedisLockerFactory implements LockerFactory {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public Locker build() {
        log.debug("构建redisLocker");
        return new RedisLocker(redisTemplate);
    }
}

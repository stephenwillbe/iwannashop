package org.linlinjava.litemall.core.lock.locker;

/**
 * @author cjh
 * @Date: 2021/12/7
 */
public interface Locker {

    long TIMEOUT_MILLIS = 3000L;
    int RETRY_TIMES = 6;
    long SLEEP_MILLIS = 500L;

    boolean lock(String key);

    boolean lock(String key, int retryTimes);

    boolean lock(String key, int retryTimes, long sleepMillis);

    boolean lock(String key, long expire);

    boolean lock(String key, long expire, int retryTimes);

    boolean lock(String key, long expire, int retryTimes, long sleepMillis);

    boolean release(String key);


}

package org.linlinjava.litemall.core.wx.pay.v3.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.WxMaConfig;
import cn.binarywang.wx.miniapp.config.WxMaInMemoryConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix="wx.pay")
@EnableConfigurationProperties
@Configuration
public class WXConfig
{
  private String appId;
  private String appSecret;
  private String mchId;
  private String notifyUrl;
  private String apiKey;
  private String apiv3Key;
  private String serialNo;
  private String privateKey;
  private String privateCertPath;

    @Bean
    public WxMaConfig wxMaConfig() {
        WxMaInMemoryConfig config = new WxMaInMemoryConfig();
        config.setAppid(appId);
        config.setSecret(appSecret);
        return config;
    }

    @Bean
    public WxMaService wxMaService(WxMaConfig maConfig) {
        WxMaService service = new WxMaServiceImpl();
        service.setWxMaConfig(maConfig);
        return service;
    }
  
  public String getAppId()
  {
    return this.appId;
  }
  
  public void setAppId(String appId)
  {
    this.appId = appId;
  }
  
  public String getMchId()
  {
    return this.mchId;
  }
  
  public void setMchId(String mchId)
  {
    this.mchId = mchId;
  }
  
  public String getNotifyUrl()
  {
    return this.notifyUrl;
  }
  
  public void setNotifyUrl(String notifyUrl)
  {
    this.notifyUrl = notifyUrl;
  }
  
  public String getApiKey()
  {
    return this.apiKey;
  }
  
  public void setApiKey(String apiKey)
  {
    this.apiKey = apiKey;
  }
  
  public String getApiv3Key()
  {
    return this.apiv3Key;
  }
  
  public void setApiv3Key(String apiv3Key)
  {
    this.apiv3Key = apiv3Key;
  }
  
  public String getSerialNo()
  {
    return this.serialNo;
  }
  
  public void setSerialNo(String serialNo)
  {
    this.serialNo = serialNo;
  }
  
  public String getPrivateKey()
  {
    return this.privateKey;
  }
  
  public void setPrivateKey(String privateKey)
  {
    this.privateKey = privateKey;
  }
  
  public String getPrivateCertPath()
  {
    return this.privateCertPath;
  }
  
  public void setPrivateCertPath(String privateCertPath)
  {
    this.privateCertPath = privateCertPath;
  }
  
  public String getAppSecret()
  {
    return this.appSecret;
  }
  
  public void setAppSecret(String appSecret)
  {
    this.appSecret = appSecret;
  }
}

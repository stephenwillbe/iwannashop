package org.linlinjava.litemall.admin.service;

import cn.hutool.core.lang.Assert;
import org.linlinjava.litemall.admin.util.AdminResponseCode;
import org.linlinjava.litemall.admin.web.AdminAftersaleController;
import org.linlinjava.litemall.core.domain.ExampleResult;
import org.linlinjava.litemall.core.notify.NotifyService;
import org.linlinjava.litemall.core.notify.NotifyType;
import org.linlinjava.litemall.core.util.JacksonUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.core.wx.pay.v3.event.WechatPayV3RefundResultEvent;
import org.linlinjava.litemall.core.wx.pay.v3.service.WechatPayV3Service;
import org.linlinjava.litemall.db.domain.LitemallAftersale;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallOrderGoods;
import org.linlinjava.litemall.db.service.LitemallAftersaleService;
import org.linlinjava.litemall.db.service.LitemallGoodsProductService;
import org.linlinjava.litemall.db.service.LitemallOrderGoodsService;
import org.linlinjava.litemall.db.service.LitemallOrderService;
import org.linlinjava.litemall.db.util.AftersaleConstant;
import org.linlinjava.litemall.db.wx.model.RefundBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import static org.linlinjava.litemall.admin.util.AdminResponseCode.ORDER_REFUND_FAILED;

/**
 * 管理端订单售后服务
 *
 * @author chenjh
 * @className AdminOrderRefundService
 * @date 2022/3/20
 */
@Service
@Transactional
public class AdminOrderRefundService {


    private final Logger logger = LoggerFactory.getLogger(AdminAftersaleController.class);

    @Autowired
    private LitemallAftersaleService aftersaleService;
    @Autowired
    private LitemallOrderService orderService;
    @Autowired
    private LitemallOrderGoodsService orderGoodsService;
    @Autowired
    private LitemallGoodsProductService goodsProductService;
    @Autowired
    private LogHelper logHelper;
    @Autowired
    private WechatPayV3Service wechatPayV3Service;
    @Autowired
    private NotifyService notifyService;

    /**
     * 监听到微信退款事件
     */
    @Transactional
    @EventListener()
    public void listenWxOrderRefundEvent(WechatPayV3RefundResultEvent refundEvent) {
        logger.info("监听到微信退款成功事件 {}",refundEvent.getRefundData().toString());
        refundSuccess(refundEvent.getRefundData().getOutRefundNo());
    }


    /**
     * 退款成功处理
     *
     * @param outRefundNo
     */
    private void refundSuccess(String outRefundNo) {
        LitemallAftersale aftersale = aftersaleService.findByOutRefundSn(outRefundNo);
        Assert.notNull(aftersale,"退款异常，找不到售后单，单号:"+outRefundNo);

        LitemallOrder order = orderService.findById(aftersale.getOrderId());
        Assert.notNull(aftersale,"退款异常，找不到订单，单号:"+aftersale.getOrderId());

        //退款成功售后处理
        aftersale.setStatus(AftersaleConstant.STATUS_REFUND);
        aftersale.setHandleTime(LocalDateTime.now());
        aftersaleService.updateById(aftersale);

        orderService.updateAftersaleStatus(aftersale.getOrderId(), AftersaleConstant.STATUS_REFUND);

        // NOTE
        // 如果是“退货退款”类型的售后，这里退款说明用户的货已经退回，则需要商品货品数量增加
        // 开发者也可以删除一下代码，在其他地方增加商品货品入库操作
        if(aftersale.getType().equals(AftersaleConstant.TYPE_GOODS_REQUIRED)) {
            List<LitemallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(aftersale.getOrderId());
            for (LitemallOrderGoods orderGoods : orderGoodsList) {
                Integer productId = orderGoods.getProductId();
                Short number = orderGoods.getNumber();
                goodsProductService.addStock(productId, number);
            }
        }

        // 发送短信通知，这里采用异步发送
        // 退款成功通知用户, 例如“您申请的订单退款 [ 单号:{1} ] 已成功，请耐心等待到账。”
        // TODO 注意订单号只发后6位
        notifyService.notifySmsTemplate(order.getMobile(), NotifyType.REFUND,
                new String[]{order.getOrderSn().substring(8, 14)});

        logHelper.logOrderSucceed("退款", "订单编号 " + order.getOrderSn() + " 售后编号 " + aftersale.getAftersaleSn());
    }


    public Object list(Integer orderId, String aftersaleSn, Short status, Integer page, Integer limit, String sort, String order) {
        List<LitemallAftersale> aftersaleList = aftersaleService.querySelective(orderId, aftersaleSn, status, page, limit, sort, order);
        return ResponseUtil.okList(aftersaleList);
    }


    public Object recept(LitemallAftersale aftersale) {
        Integer id = aftersale.getId();
        LitemallAftersale aftersaleOne = aftersaleService.findById(id);
        if(aftersaleOne == null){
            return ResponseUtil.fail(AdminResponseCode.AFTERSALE_NOT_ALLOWED, "售后不存在");
        }
        Short status = aftersaleOne.getStatus();
        if(!status.equals(AftersaleConstant.STATUS_REQUEST)){
            return ResponseUtil.fail(AdminResponseCode.AFTERSALE_NOT_ALLOWED, "售后不能进行审核通过操作");
        }
        aftersaleOne.setStatus(AftersaleConstant.STATUS_RECEPT);
        aftersaleOne.setHandleTime(LocalDateTime.now());
        aftersaleService.updateById(aftersaleOne);

        // 订单也要更新售后状态
        orderService.updateAftersaleStatus(aftersaleOne.getOrderId(), AftersaleConstant.STATUS_RECEPT);
        return ResponseUtil.ok();
    }


    public Object batchRecept(String body) {
        List<Integer> ids = JacksonUtil.parseIntegerList(body, "ids");
        // NOTE
        // 批量操作中，如果一部分数据项失败，应该如何处理
        // 这里采用忽略失败，继续处理其他项。
        // 当然开发者可以采取其他处理方式，具体情况具体分析，例如利用事务回滚所有操作然后返回用户失败信息
        for(Integer id : ids) {
            LitemallAftersale aftersale = aftersaleService.findById(id);
            if(aftersale == null){
                continue;
            }
            Short status = aftersale.getStatus();
            if(!status.equals(AftersaleConstant.STATUS_REQUEST)){
                continue;
            }
            aftersale.setStatus(AftersaleConstant.STATUS_RECEPT);
            aftersale.setHandleTime(LocalDateTime.now());
            aftersaleService.updateById(aftersale);

            // 订单也要更新售后状态
            orderService.updateAftersaleStatus(aftersale.getOrderId(), AftersaleConstant.STATUS_RECEPT);
        }
        return ResponseUtil.ok();
    }



    public Object reject(LitemallAftersale aftersale) {
        Integer id = aftersale.getId();
        LitemallAftersale aftersaleOne = aftersaleService.findById(id);
        if(aftersaleOne == null){
            return ResponseUtil.badArgumentValue();
        }
        Short status = aftersaleOne.getStatus();
        if(!status.equals(AftersaleConstant.STATUS_REQUEST)){
            return ResponseUtil.fail(AdminResponseCode.AFTERSALE_NOT_ALLOWED, "售后不能进行审核拒绝操作");
        }
        aftersaleOne.setStatus(AftersaleConstant.STATUS_REJECT);
        aftersaleOne.setHandleTime(LocalDateTime.now());
        aftersaleService.updateById(aftersaleOne);

        // 订单也要更新售后状态
        orderService.updateAftersaleStatus(aftersaleOne.getOrderId(), AftersaleConstant.STATUS_REJECT);
        return ResponseUtil.ok();
    }




    public Object batchReject(String body) {
        List<Integer> ids = JacksonUtil.parseIntegerList(body, "ids");
        for(Integer id : ids) {
            LitemallAftersale aftersale = aftersaleService.findById(id);
            if(aftersale == null){
                continue;
            }
            Short status = aftersale.getStatus();
            if(!status.equals(AftersaleConstant.STATUS_REQUEST)){
                continue;
            }
            aftersale.setStatus(AftersaleConstant.STATUS_REJECT);
            aftersale.setHandleTime(LocalDateTime.now());
            aftersaleService.updateById(aftersale);

            // 订单也要更新售后状态
            orderService.updateAftersaleStatus(aftersale.getOrderId(), AftersaleConstant.STATUS_REJECT);
        }
        return ResponseUtil.ok();
    }


    /**
     * 同意退款
     * @param userId 用户id
     * @param orderId 订单id
     * @return
     */
    public Object refundByUserIdAndOrderId(Integer userId,Integer orderId) {
        LitemallAftersale aftersale = aftersaleService.findByOrderId(userId, orderId);
        return this.refund(aftersale.getId());
    }

    /**
     * 同意退款
     * 向微信发起退款申请
     * @param afterSaleId
     * @return
     */
    public Object refund(Integer afterSaleId) {
        LitemallAftersale aftersaleOne = aftersaleService.findById(afterSaleId);
        if(aftersaleOne == null){
            return ResponseUtil.badArgumentValue();
        }
        if(!aftersaleOne.getStatus().equals(AftersaleConstant.STATUS_RECEPT)){
            return ResponseUtil.fail(AdminResponseCode.AFTERSALE_NOT_ALLOWED, "售后不能进行退款操作");
        }

        //微信退款请求
        try {
            ExampleResult<RefundBody> r = wechatPayV3Service.refundApply(aftersaleOne.getAftersaleSn());
            return r.isOk() ? ResponseUtil.ok() : ResponseUtil.fail(ORDER_REFUND_FAILED, "订单退款失败");
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            return ResponseUtil.fail(ORDER_REFUND_FAILED, "订单退款失败");
        }


//        // 微信退款
//        WxPayRefundRequest wxPayRefundRequest = new WxPayRefundRequest();
//        wxPayRefundRequest.setOutTradeNo(order.getOrderSn());
//        wxPayRefundRequest.setOutRefundNo("refund_" + order.getOrderSn());
//        // 元转成分
//        Integer totalFee = aftersaleOne.getAmount().multiply(new BigDecimal(100)).intValue();
//        wxPayRefundRequest.setTotalFee(order.getActualPrice().multiply(new BigDecimal(100)).intValue());
//        wxPayRefundRequest.setRefundFee(totalFee);
//
//
//        WxPayRefundResult wxPayRefundResult;
//        try {
//            wxPayRefundResult = wxPayService.refund(wxPayRefundRequest);
//        } catch (WxPayException e) {
//            logger.error(e.getMessage(), e);
//            return ResponseUtil.fail(ORDER_REFUND_FAILED, "订单退款失败");
//        }
//        if (!wxPayRefundResult.getReturnCode().equals("SUCCESS")) {
//            logger.warn("refund fail: " + wxPayRefundResult.getReturnMsg());
//            return ResponseUtil.fail(ORDER_REFUND_FAILED, "订单退款失败");
//        }
//        if (!wxPayRefundResult.getResultCode().equals("SUCCESS")) {
//            logger.warn("refund fail: " + wxPayRefundResult.getReturnMsg());
//            return ResponseUtil.fail(ORDER_REFUND_FAILED, "订单退款失败");
//        }

//        //退款成功
//        aftersaleOne.setStatus(AftersaleConstant.STATUS_REFUND);
//        aftersaleOne.setHandleTime(LocalDateTime.now());
//        aftersaleService.updateById(aftersaleOne);
//
//        orderService.updateAftersaleStatus(orderId, AftersaleConstant.STATUS_REFUND);
//
//        // NOTE
//        // 如果是“退货退款”类型的售后，这里退款说明用户的货已经退回，则需要商品货品数量增加
//        // 开发者也可以删除一下代码，在其他地方增加商品货品入库操作
//        if(aftersale.getType().equals(AftersaleConstant.TYPE_GOODS_REQUIRED)) {
//            List<LitemallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(orderId);
//            for (LitemallOrderGoods orderGoods : orderGoodsList) {
//                Integer productId = orderGoods.getProductId();
//                Short number = orderGoods.getNumber();
//                goodsProductService.addStock(productId, number);
//            }
//        }
//
//        // 发送短信通知，这里采用异步发送
//        // 退款成功通知用户, 例如“您申请的订单退款 [ 单号:{1} ] 已成功，请耐心等待到账。”
//        // TODO 注意订单号只发后6位
//        notifyService.notifySmsTemplate(order.getMobile(), NotifyType.REFUND,
//                new String[]{order.getOrderSn().substring(8, 14)});
//
//        logHelper.logOrderSucceed("退款", "订单编号 " + order.getOrderSn() + " 售后编号 " + aftersale.getAftersaleSn());
//        return ResponseUtil.ok();
    }

}
